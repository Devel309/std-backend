package pe.gob.muni.stdbackend.negocio.modelos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListLoginDto {
	
	private Integer idUsuario;
	private String apNombres;
	private Integer idArea;
	private String area;
	private Integer idCargo;
	private String cargo;
	private Integer indicador;
}
