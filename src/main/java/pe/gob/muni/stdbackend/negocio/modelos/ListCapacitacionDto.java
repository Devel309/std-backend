package pe.gob.muni.stdbackend.negocio.modelos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListCapacitacionDto {
	
	private String fundamentacion;
	private String lugarSalida;
	private String lugarDesino;
	private String fRegistro;
	private String descripcionpdf;
	private String pdfSustento;
	private String fSalida;
	private String fRetorno;
	private Integer idTema;
	private String tema;
	private Integer idInstitucion;
	private String nombre;
}