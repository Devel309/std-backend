package pe.gob.muni.stdbackend.negocio.modelos;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Aplicacion extends ModeloBase{
	private Integer idAplicacion;
	private String nombre;
	private String descripcion;
	private String codigo;
	private String url;

	private List<Aplicacion> aplicaciones = new ArrayList<>();
	private Aplicacion aplicacion;
//	private UnidadOrganica unidadOrganica;
}
