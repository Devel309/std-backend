package pe.gob.muni.stdbackend.negocio.servicios.implementacion;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.muni.stdbackend.negocio.modelos.BuscarLoginDto;
import pe.gob.muni.stdbackend.negocio.modelos.ListLoginDto;
import pe.gob.muni.stdbackend.negocio.repositorios.mappers.LoginMapper;
import pe.gob.muni.stdbackend.negocio.servicios.interfaces.LoginService;
import pe.gob.muni.stdbackend.utils.Constant;


@Service("loginService")
public class LoginServiceImpl implements LoginService{

	@Autowired
	LoginMapper loginMapper;
	
	@Override
	public List<ListLoginDto> listLogin(BuscarLoginDto param) throws Exception{
		try {
			Map responseMap = new HashMap<>();
			//System.out.println("aqui: "+param.getIdTipoExp() + "\n"+param.getDni());
			responseMap.put("user",param.getUser());
			responseMap.put("password",param.getPassword());
			return loginMapper.listLogin(responseMap);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(Constant.MSJ_ERROR);
		}
	}
	
	@Override
	@Transactional (propagation = Propagation.REQUIRED,isolation = Isolation.DEFAULT,timeout=36000, rollbackFor =Exception.class)
	public void updatePassword(HashMap<Object, Object> ltp) throws Exception {
		System.out.println("services:  " +ltp);
		try {
			loginMapper.updatePassword(ltp);	
			System.out.println("ltp");
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(Constant.MSJ_ERROR);
		}
	}
}
