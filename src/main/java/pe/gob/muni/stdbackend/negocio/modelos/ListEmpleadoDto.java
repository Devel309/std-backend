package pe.gob.muni.stdbackend.negocio.modelos;

import org.apache.ibatis.type.Alias;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class ListEmpleadoDto {
	
	public Integer dni;
	public String apellidos;
	public String nombres;
	public String ruc;
	public String razonSocial;
	public String direccion;
	public String fecha;
	public Integer idArea;
	public String area;
	public Integer idCargo;
	public String cargo;
	public String correlativo;
	public String tdr;
	public String fechaInicio;
	public String fechaFin;
	public Integer celular;
	public String correo;
	public String descripcion;
	public Integer idPersona;
	public Integer idEmpleado;
	public String usuario;
	
	public String fNacimiento;
}
