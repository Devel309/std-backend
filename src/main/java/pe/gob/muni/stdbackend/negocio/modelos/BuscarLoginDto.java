package pe.gob.muni.stdbackend.negocio.modelos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BuscarLoginDto {
	private String user;
	private String password;
}
