package pe.gob.muni.stdbackend.negocio.modelos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PapeletaDto {

	private String lugarSalida;
	private String lugarDestino;
	private String fundamentacion;
	private String descripcionPDF;
	private String pdSustento;
	private String fSalida;
	private String fRetorno;
	private Integer idMotivo;
	private Integer idInstitucion;
	private Integer idEmpleado;
	
	private String respuesta;
}
