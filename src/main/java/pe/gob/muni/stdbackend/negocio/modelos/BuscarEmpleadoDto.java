package pe.gob.muni.stdbackend.negocio.modelos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BuscarEmpleadoDto {
	public Integer dni;
	public String apNombres;
}	
