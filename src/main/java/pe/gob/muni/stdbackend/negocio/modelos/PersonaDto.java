package pe.gob.muni.stdbackend.negocio.modelos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PersonaDto {
	
	public Integer idPersona;
	public Integer dni;
	public String apellidos;
	public String nombres;
	public String ruc;
	public String razonSocial;
	public String direccion;
	public String fechNacimiento;
	
	public Integer respuesta;
}
