package pe.gob.muni.stdbackend.negocio.modelos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListPapeletaDto {
	
	private String lugarSalida;
	private String lugarDestino;
	private String fundamentacion;
	private String fecha;
	private String descripcionPDF;
	private String pdfSustento;
	private String fSalida;
	private String fRetorno;
	private Integer idMotivo;
	private String motivo;
	private Integer idInstitucion;
	private String nombre;
	private String empleado;
}
 