package pe.gob.muni.stdbackend.negocio.servicios.interfaces;

import java.util.HashMap;
import java.util.List;

import pe.gob.muni.stdbackend.negocio.modelos.BuscarLoginDto;
import pe.gob.muni.stdbackend.negocio.modelos.ListLoginDto;


public interface LoginService {


	public List<ListLoginDto> listLogin(BuscarLoginDto param) throws Exception;

	public void updatePassword(HashMap<Object, Object> ltp) throws Exception;

}
