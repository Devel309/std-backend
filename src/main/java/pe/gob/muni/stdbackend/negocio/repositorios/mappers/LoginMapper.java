package pe.gob.muni.stdbackend.negocio.repositorios.mappers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import pe.gob.muni.stdbackend.negocio.modelos.ListLoginDto;


@Mapper
public interface LoginMapper {
	
	public List<ListLoginDto> listLogin(Map response) throws Exception;
	
	public void updatePassword(HashMap<Object, Object> ltp);
}
