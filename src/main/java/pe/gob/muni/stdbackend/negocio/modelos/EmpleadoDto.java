package pe.gob.muni.stdbackend.negocio.modelos;

import java.util.Date;

import org.apache.ibatis.type.Alias;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class EmpleadoDto {
	
	//actualizar
	public Integer idEmpleado;//
	
	public Integer idPersona;//
	public Integer idArea;//
	public Integer idCargo;//
	public String correlativo;//
	public String tdr;//
	public String fechaInicio;//
	public String fechafin;//
	public String login;//
	public Integer celular;//
	public String correo;//
	public String descripcion;//
	
	
	//respuesta
	public Integer respuesta;//
	
}
