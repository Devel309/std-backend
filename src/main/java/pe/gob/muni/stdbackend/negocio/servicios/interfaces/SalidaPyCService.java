package pe.gob.muni.stdbackend.negocio.servicios.interfaces;

import java.util.HashMap;
import java.util.List;

import pe.gob.muni.stdbackend.negocio.modelos.ListCapacitacionDto;
import pe.gob.muni.stdbackend.negocio.modelos.ListPapeletaDto;


public interface SalidaPyCService {

	public void createPapeleta(HashMap<Object, Object> ltp) throws Exception;

	public List<ListPapeletaDto> listPapeleta() throws Exception;

	public void createCapacitacion(HashMap<Object, Object> ltp) throws Exception;

	public List<ListCapacitacionDto> listCapacitacion() throws Exception;

}
