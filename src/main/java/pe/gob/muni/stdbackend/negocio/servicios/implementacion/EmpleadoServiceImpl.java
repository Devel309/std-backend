package pe.gob.muni.stdbackend.negocio.servicios.implementacion;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.muni.stdbackend.negocio.modelos.BuscarEmpleadoDto;
import pe.gob.muni.stdbackend.negocio.modelos.ListEmpleadoDto;
import pe.gob.muni.stdbackend.negocio.repositorios.mappers.EmpleadoMapper;
import pe.gob.muni.stdbackend.negocio.servicios.interfaces.EmpleadoService;
import pe.gob.muni.stdbackend.utils.Constant;



@Service("empleadoService")
public class EmpleadoServiceImpl implements EmpleadoService{
	
	@Autowired
	private EmpleadoMapper empleadoMapper;
	
	@Override
	@Transactional (propagation = Propagation.REQUIRED,isolation = Isolation.DEFAULT,timeout=36000, rollbackFor =Exception.class)
	public void createRiesgo(HashMap<Object, Object> ltp) throws Exception {
		System.out.println("services:  " +ltp);
		try {
			empleadoMapper.createEmpleado(ltp);	
			System.out.println("ltp"+ltp);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(Constant.MSJ_ERROR);
		}
	}
	@Override
	public List<ListEmpleadoDto> listEmpleado(BuscarEmpleadoDto param) throws Exception{
		try {
			Map responseMap = new HashMap<>();
			System.out.println("aqui: "+param.getApNombres() + "\n"+param.getDni());
			responseMap.put("apNombres",param.getApNombres());
			responseMap.put("dni",param.getDni());
			
			System.out.println("response"+empleadoMapper.listEmpleado(responseMap));
			
			return empleadoMapper.listEmpleado(responseMap);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(Constant.MSJ_ERROR);
		}
	}
	@Override
	@Transactional (propagation = Propagation.REQUIRED,isolation = Isolation.DEFAULT,timeout=36000, rollbackFor =Exception.class)
	public void updateEmpleado(HashMap<Object, Object> ltp) throws Exception {
		System.out.println("services:  " +ltp);
		try {
			empleadoMapper.updateEmpleado(ltp);	
			System.out.println("ltp");
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(Constant.MSJ_ERROR);
		}
	}
	
	
}
