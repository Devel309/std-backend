package pe.gob.muni.stdbackend.negocio.modelos;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListExpedienteDto {

	private Integer idExpediente;
	private String correlativo;
	private String asuntoAnexo;
	private String contenidoExp;
	private Date fechaRegistro;
	private String pdfExp;
	private String pdfAnexo;
	private String estado;
}
