package pe.gob.muni.stdbackend.negocio.servicios.implementacion;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.muni.stdbackend.negocio.modelos.BuscarEmpleadoDto;
import pe.gob.muni.stdbackend.negocio.modelos.ListPersonaDto;
import pe.gob.muni.stdbackend.negocio.repositorios.mappers.PersonaMapper;
import pe.gob.muni.stdbackend.negocio.servicios.interfaces.PersonaService;
import pe.gob.muni.stdbackend.utils.Constant;



@Service("personaService")
public class PersonaServiceImpl implements PersonaService{
	
	@Autowired
	private PersonaMapper personaMapper;
	
	@Override
	@Transactional (propagation = Propagation.REQUIRED,isolation = Isolation.DEFAULT,timeout=36000, rollbackFor =Exception.class)
	public void createPersona(HashMap<Object, Object> ltp) throws Exception {
		System.out.println("services12:  " +ltp);
		try {
			personaMapper.createPersona(ltp);	
			System.out.println("ltp"+ltp);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(Constant.MSJ_ERROR);
		}
	}
	
	@Override
	public List<ListPersonaDto> listPersona(BuscarEmpleadoDto param) throws Exception{
		try {
			Map responseMap = new HashMap<>();
			System.out.println("aqui: "+param.getApNombres() + "\n"+param.getDni());
			responseMap.put("apNombres",param.getApNombres());
			responseMap.put("dni",param.getDni());
			
			System.out.println("response"+personaMapper.listPersona(responseMap));
			
			return personaMapper.listPersona(responseMap);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(Constant.MSJ_ERROR);
		}
	}
	@Override
	@Transactional (propagation = Propagation.REQUIRED,isolation = Isolation.DEFAULT,timeout=36000, rollbackFor =Exception.class)
	public void updatePersona(HashMap<Object, Object> ltp) throws Exception {
		System.out.println("services12:  " +ltp);
		try {
			personaMapper.updatePersona(ltp);	
			System.out.println("ltp"+ltp);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(Constant.MSJ_ERROR);
		}
	}
	
}
