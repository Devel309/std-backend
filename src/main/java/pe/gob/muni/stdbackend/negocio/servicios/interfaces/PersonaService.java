package pe.gob.muni.stdbackend.negocio.servicios.interfaces;

import java.util.HashMap;
import java.util.List;

import pe.gob.muni.stdbackend.negocio.modelos.BuscarEmpleadoDto;
import pe.gob.muni.stdbackend.negocio.modelos.ListPersonaDto;



public interface PersonaService {

	public void createPersona(HashMap<Object, Object> ltp) throws Exception;

	public List<ListPersonaDto> listPersona(BuscarEmpleadoDto param) throws Exception;

	public void updatePersona(HashMap<Object, Object> ltp) throws Exception;

	
}
