package pe.gob.muni.stdbackend.negocio.repositorios.mappers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import pe.gob.muni.stdbackend.negocio.modelos.ListPersonaDto;


@Mapper
public interface PersonaMapper {
	public void createPersona(HashMap<Object, Object> ltp);
	
	public List<ListPersonaDto> listPersona(Map response) throws Exception;
	
	public void updatePersona(HashMap<Object, Object> ltp);
}
