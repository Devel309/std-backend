package pe.gob.muni.stdbackend.negocio.repositorios.mappers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UsuarioMapper {
	
	public void listUsuario(HashMap<Object, Object> ltp) throws Exception;
}
