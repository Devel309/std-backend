package pe.gob.muni.stdbackend.negocio.modelos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ActContraseñaUsuarioDto {
	private Integer idUsuario;
	private String password;
	
	private Integer respuesta;
}
