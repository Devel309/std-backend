package pe.gob.muni.stdbackend.negocio.servicios.interfaces;

import java.util.HashMap;
import java.util.List;

import pe.gob.muni.stdbackend.negocio.modelos.BuscarExpedienteDto;
import pe.gob.muni.stdbackend.negocio.modelos.ListExpedienteDto;
import pe.gob.muni.stdbackend.negocio.modelos.ListSeguimientoExternoDto;
import pe.gob.muni.stdbackend.negocio.modelos.ListVerDetalleExpedienteDto;


public interface ExpedienteService {

	public void createExpediente(HashMap<Object, Object> ltp) throws Exception;

	public List<ListExpedienteDto> listExpediente(BuscarExpedienteDto param) throws Exception;

	public void derivarExpediente(HashMap<Object, Object> ltp) throws Exception;

	public List<ListVerDetalleExpedienteDto> listverDetalleExpedienteDto(Integer idExpediente) throws Exception;
	
	public List<ListSeguimientoExternoDto> listSeguiExpExt(HashMap<Object, Object> ltp) throws Exception;
}
