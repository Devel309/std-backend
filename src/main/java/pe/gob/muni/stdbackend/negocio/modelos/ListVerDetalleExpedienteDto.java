package pe.gob.muni.stdbackend.negocio.modelos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListVerDetalleExpedienteDto {
	private String area;
	private String fecha;
	private String descripcion;
}
