package pe.gob.muni.stdbackend.negocio.servicios.interfaces;

import java.util.List;

import pe.gob.muni.stdbackend.negocio.modelos.LisEmpleadoPersonaDto;
import pe.gob.muni.stdbackend.negocio.modelos.ListComboAreaDto;
import pe.gob.muni.stdbackend.negocio.modelos.ListComboCargoDto;
import pe.gob.muni.stdbackend.negocio.modelos.ListMotivoInstitucionDto;


public interface ListComboService {

	public List<ListComboAreaDto> listComboArea() throws Exception;

	public List<ListComboCargoDto> listComboCargo() throws Exception;

	public List<ListMotivoInstitucionDto> listComboMotivo() throws Exception;

	public List<ListMotivoInstitucionDto> listComboTema() throws Exception;

	public List<ListMotivoInstitucionDto> listInstitucion(ListMotivoInstitucionDto param) throws Exception;

	public List<LisEmpleadoPersonaDto> lisEmpleadoPersona(ListMotivoInstitucionDto param) throws Exception;


}
