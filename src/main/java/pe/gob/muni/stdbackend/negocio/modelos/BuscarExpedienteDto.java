package pe.gob.muni.stdbackend.negocio.modelos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BuscarExpedienteDto {

	private Integer idTipoExp;
	private String correlativo;
	private String dni;
	private String fechaInicio;
	private String fechaFin;
	private String area;
	private Integer idArea;
}
