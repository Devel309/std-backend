package pe.gob.muni.stdbackend.negocio.modelos;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ListSeguimientoExternoDto {
	
	private String area;
	private String fecha;
	private String descripcion;
	private String correlativo;
	private String fRegistro;
	private String asunto;
	private Integer dni;
	private String nombres;
	private String estado;
	
}
