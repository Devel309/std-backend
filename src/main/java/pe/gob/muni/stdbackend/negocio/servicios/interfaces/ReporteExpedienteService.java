package pe.gob.muni.stdbackend.negocio.servicios.interfaces;

import java.util.HashMap;
import java.util.List;

import pe.gob.muni.stdbackend.negocio.modelos.BuscarExpedienteDto;


public interface ReporteExpedienteService {

	public HashMap<String, Object> listReporte(BuscarExpedienteDto param) throws Exception;


}
