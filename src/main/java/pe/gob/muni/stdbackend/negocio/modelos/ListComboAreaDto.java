package pe.gob.muni.stdbackend.negocio.modelos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListComboAreaDto {
	private Integer id;
	private	String area;
	private	String descripcion;
}
