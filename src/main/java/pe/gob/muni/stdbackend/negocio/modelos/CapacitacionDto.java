package pe.gob.muni.stdbackend.negocio.modelos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CapacitacionDto {
	private String fundamentacion;
	private String lugarSalida;
	private String lugarDestino;
	private String descripPDF;
	private String pdfSustento;
	private String fSalida;
	private String fRetorno;
	private Integer idTema;
	private Integer idInstitucion;
	
	private String respuesta;
}
