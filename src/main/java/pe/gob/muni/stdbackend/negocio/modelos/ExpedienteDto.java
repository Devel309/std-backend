package pe.gob.muni.stdbackend.negocio.modelos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExpedienteDto {

	public String tipo;
	public String asuntoExp;
	public String contenidoExp;
	public String descripcionPdf;
	public String pdfExp;
	public Integer folio;
	public String descripcionAnexo;
	public String pdfAnexo;
	public Integer idUsuario;
	public Integer idEstadoExp;
	public Integer idTipoExp;
	public Integer idPersona;
	public Long carnetExt;
	public Integer celular;
	public String correo;
	public String descripcion;
	
	public String respuesta;
	
}
