package pe.gob.muni.stdbackend.negocio.servicios.implementacion;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.muni.stdbackend.negocio.modelos.ListCapacitacionDto;
import pe.gob.muni.stdbackend.negocio.modelos.ListPapeletaDto;
import pe.gob.muni.stdbackend.negocio.repositorios.mappers.SalidaPyCMapper;
import pe.gob.muni.stdbackend.negocio.servicios.interfaces.SalidaPyCService;
import pe.gob.muni.stdbackend.utils.Constant;



@Service("salidaService")
public class SalidaPyCServiceImpl implements SalidaPyCService{

	@Autowired
	SalidaPyCMapper salidaPyCMapper;
	
	@Override
	@Transactional (propagation = Propagation.REQUIRED,isolation = Isolation.DEFAULT,timeout=36000, rollbackFor =Exception.class)
	public void createPapeleta(HashMap<Object, Object> ltp) throws Exception {
		System.out.println("services12:  " +ltp);
		try {
			salidaPyCMapper.createPapeleta(ltp);	
			System.out.println("ltp"+ltp);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(Constant.MSJ_ERROR);
		}
	}
		
	
	@Override
	public List<ListPapeletaDto> listPapeleta() throws Exception{
		try {
			Map responseMap = new HashMap<>();
			return salidaPyCMapper.listPapeleta(responseMap);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(Constant.MSJ_ERROR);
		}
	}
	
	@Override
	@Transactional (propagation = Propagation.REQUIRED,isolation = Isolation.DEFAULT,timeout=36000, rollbackFor =Exception.class)
	public void createCapacitacion(HashMap<Object, Object> ltp) throws Exception {
		System.out.println("services12:  " +ltp);
		try {
			salidaPyCMapper.createCapacitacion(ltp);	
			System.out.println("ltp"+ltp);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(Constant.MSJ_ERROR);
		}
	}
		
	
	@Override
	public List<ListCapacitacionDto> listCapacitacion() throws Exception{
		try {
			Map responseMap = new HashMap<>();
			return salidaPyCMapper.listCapacitacion(responseMap);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(Constant.MSJ_ERROR);
		}
	}
}
