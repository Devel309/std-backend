package pe.gob.muni.stdbackend.negocio.modelos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LisEmpleadoPersonaDto {
	private Integer id;
	private Integer dni;
	private String apellidos;
	private String nombres;
}
