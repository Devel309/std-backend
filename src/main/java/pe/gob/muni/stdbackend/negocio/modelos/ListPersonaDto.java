package pe.gob.muni.stdbackend.negocio.modelos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListPersonaDto {
	
	private Integer idPersona;
	private Integer dni;
	private String apellidos;
	private String nombres;
	private String ruc;
	private String razonSocial;
	private String direccion;
	
}
