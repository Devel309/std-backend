package pe.gob.muni.stdbackend.negocio.modelos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListComboCargoDto {
	private Integer id;
	private String cargo;
	private String remuneracion;
	private String descripcion;
}
