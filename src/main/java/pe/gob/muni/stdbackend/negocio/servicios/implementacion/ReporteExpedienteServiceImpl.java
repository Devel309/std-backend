package pe.gob.muni.stdbackend.negocio.servicios.implementacion;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.muni.stdbackend.negocio.modelos.BuscarExpedienteDto;
import pe.gob.muni.stdbackend.negocio.modelos.ListExpedienteDto;
import pe.gob.muni.stdbackend.negocio.modelos.ListVerDetalleExpedienteDto;
import pe.gob.muni.stdbackend.negocio.modelos.ReporteExpedientesDto;
import pe.gob.muni.stdbackend.negocio.repositorios.mappers.ExpedienteMapper;
import pe.gob.muni.stdbackend.negocio.servicios.interfaces.ReporteExpedienteService;
import pe.gob.muni.stdbackend.utils.Constant;


@Service("reporteService")
public class ReporteExpedienteServiceImpl implements ReporteExpedienteService{

	@Autowired
	ExpedienteMapper expedienteMapper;
	
	@Override
	public HashMap<String, Object> listReporte(BuscarExpedienteDto param) throws Exception{
		try {
			List<ReporteExpedientesDto> res = new ArrayList<>();
			List<ListVerDetalleExpedienteDto> resp = new ArrayList<>();
			List<ListExpedienteDto> response = new ArrayList<>();
			Map responseMap = new HashMap<>();
			System.out.println("aqui: "+param.getIdTipoExp() + "\n"+param.getDni());
			responseMap.put("idTipoExp",param.getIdTipoExp());
			responseMap.put("correlativo",param.getCorrelativo());
			responseMap.put("dni",param.getDni());
			responseMap.put("fechaInicio",param.getFechaInicio());
			responseMap.put("fechaFin",param.getFechaFin());
			responseMap.put("area",param.getArea());
			responseMap.put("idArea",param.getIdArea());
			response = expedienteMapper.listExpediente(responseMap);
			
			for (int i=0; i < response.size(); i++) {
				
				ReporteExpedientesDto rs = new ReporteExpedientesDto();
				rs.setIdExpediente(response.get(i).getIdExpediente());
				rs.setCorrelativo("Exp: "+response.get(i).getCorrelativo());
				rs.setAsuntoAnexo(response.get(i).getAsuntoAnexo());
				rs.setContenidoExp(response.get(i).getContenidoExp());
				rs.setFechaRegistro(response.get(i).getFechaRegistro());
				//rs.setPdfExp(response.get(i).getPdfExp());
				//rs.setPdfAnexo(response.get(i).getPdfAnexo());
				rs.setEstado(response.get(i).getEstado());
				rs.setDetalle( resp = expedienteMapper.listverDetalleExpedienteDto(response.get(i).getIdExpediente()));
				res.add(rs);
				System.out.println("En iif getIdResponsable es..."+response.get(i).getIdExpediente());
			}
			String html=""; 
			HashMap<String, Object> context = new HashMap<String, Object>();
			context.put("listaReporte", res);
			
			TemplateEngineService templateEngine = new TemplateEngineService();
			System.out.println("context:::::"+context);
			html+=templateEngine.processTemplate("pdf/reporteGeneral.html",context);
			System.out.println("html"+html);
			String base64Content= PdfGenerator.pdfBase64FromHtmString(html);
			System.out.println("Enviar a front base64Content"+base64Content);
			
			HashMap<String, Object> resultado = new HashMap<String, Object>();
			resultado.put("resultado", "data:application/pdf;base64,"+base64Content);
			
			System.out.println("resultBase64::::  "+base64Content);
			return resultado;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(Constant.MSJ_ERROR);
		}
	}
	
	
}
