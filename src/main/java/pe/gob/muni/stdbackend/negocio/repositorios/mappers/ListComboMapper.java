package pe.gob.muni.stdbackend.negocio.repositorios.mappers;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import pe.gob.muni.stdbackend.negocio.modelos.LisEmpleadoPersonaDto;
import pe.gob.muni.stdbackend.negocio.modelos.ListComboAreaDto;
import pe.gob.muni.stdbackend.negocio.modelos.ListComboCargoDto;
import pe.gob.muni.stdbackend.negocio.modelos.ListMotivoInstitucionDto;

@Mapper
public interface ListComboMapper {

	public List<ListComboAreaDto> listComboArea(Map response) throws Exception;
	
	public List<ListComboCargoDto> listComboCargo(Map response) throws Exception;
	
	public List<ListMotivoInstitucionDto> listComboTema(Map response) throws Exception;
	
	public List<ListMotivoInstitucionDto> listComboMotivo(Map response) throws Exception;
	
	public List<ListMotivoInstitucionDto> listInstitucion(Map response) throws Exception;
	
	public List<LisEmpleadoPersonaDto> lisEmpleadoPersona(Map response) throws Exception;
	
	
}
