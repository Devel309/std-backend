package pe.gob.muni.stdbackend.negocio.servicios.implementacion;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.muni.stdbackend.negocio.modelos.LisEmpleadoPersonaDto;
import pe.gob.muni.stdbackend.negocio.modelos.ListComboAreaDto;
import pe.gob.muni.stdbackend.negocio.modelos.ListComboCargoDto;
import pe.gob.muni.stdbackend.negocio.modelos.ListMotivoInstitucionDto;
import pe.gob.muni.stdbackend.negocio.repositorios.mappers.ListComboMapper;
import pe.gob.muni.stdbackend.negocio.servicios.interfaces.ListComboService;
import pe.gob.muni.stdbackend.utils.Constant;


@Service("serviceCombo")
public class ListComboServiceImpl implements ListComboService{
	@Autowired
	ListComboMapper listComboMapper;
	
	@Override
	public List<ListComboAreaDto> listComboArea() throws Exception{
		try {
			Map responseMap = new HashMap<>();
			
			System.out.println("response"+listComboMapper.listComboArea(responseMap));
			
			return listComboMapper.listComboArea(responseMap);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(Constant.MSJ_ERROR);
		}
	}
	@Override
	public List<ListComboCargoDto> listComboCargo() throws Exception{
		try {
			Map responseMap = new HashMap<>();
			
			System.out.println("response"+listComboMapper.listComboCargo(responseMap));
			
			return listComboMapper.listComboCargo(responseMap);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(Constant.MSJ_ERROR);
		}
	}
	
	@Override
	public List<ListMotivoInstitucionDto> listComboTema() throws Exception{
		try {
			Map responseMap = new HashMap<>();
			
			System.out.println("response"+listComboMapper.listComboTema(responseMap));
			
			return listComboMapper.listComboTema(responseMap);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(Constant.MSJ_ERROR);
		}
	}
	
	@Override
	public List<ListMotivoInstitucionDto> listComboMotivo() throws Exception{
		try {
			Map responseMap = new HashMap<>();
			
			System.out.println("response"+listComboMapper.listComboCargo(responseMap));
			
			return listComboMapper.listComboMotivo(responseMap);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(Constant.MSJ_ERROR);
		}
	}
	
	@Override
	public List<ListMotivoInstitucionDto> listInstitucion(ListMotivoInstitucionDto param) throws Exception{
		try {
			Map responseMap = new HashMap<>();
			responseMap.put("desRuc",param.getDesRuc());
			System.out.println("response"+listComboMapper.listInstitucion(responseMap));
			
			return listComboMapper.listInstitucion(responseMap);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(Constant.MSJ_ERROR);
		}
	}
	@Override
	public List<LisEmpleadoPersonaDto> lisEmpleadoPersona(ListMotivoInstitucionDto param) throws Exception{
		try {
			Map responseMap = new HashMap<>();
			responseMap.put("id",param.getId());
			System.out.println("response"+listComboMapper.lisEmpleadoPersona(responseMap));
			
			return listComboMapper.lisEmpleadoPersona(responseMap);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(Constant.MSJ_ERROR);
		}
	}
	
	
}
