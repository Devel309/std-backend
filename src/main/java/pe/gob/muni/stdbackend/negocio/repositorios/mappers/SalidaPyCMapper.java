package pe.gob.muni.stdbackend.negocio.repositorios.mappers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import pe.gob.muni.stdbackend.negocio.modelos.ListCapacitacionDto;
import pe.gob.muni.stdbackend.negocio.modelos.ListPapeletaDto;



@Mapper
public interface SalidaPyCMapper {

	public List<ListPapeletaDto> listPapeleta(Map responseMap) throws Exception;
	
	public void createPapeleta(HashMap<Object, Object> ltp);
	
	public List<ListCapacitacionDto> listCapacitacion(Map responseMap) throws Exception;
	
	public void createCapacitacion(HashMap<Object, Object> ltp);
}
