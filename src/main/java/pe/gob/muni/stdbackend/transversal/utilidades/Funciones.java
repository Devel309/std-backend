package pe.gob.muni.stdbackend.transversal.utilidades;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.TypeMap;



import io.jsonwebtoken.impl.DefaultClaims;
import pe.gob.muni.stdbackend.negocio.modelos.Aplicacion;
import pe.gob.muni.stdbackend.negocio.modelos.ModeloBase;
import pe.gob.muni.stdbackend.seguridad.jwt.JWTTokenProvider;
import pe.gob.muni.stdbackend.transversal.excepciones.CustomException;
import pe.gob.muni.stdbackend.transversal.excepciones.SessionDuplicateException;
import pe.gob.muni.stdbackend.vista.dtos.output.ListarAplicacionOutputDto;



public class Funciones {

	static ModelMapper modelMapper = new ModelMapper();
	



	public static void configuraAplicacionToListarAplicacionOutputDtoMapper() {
		TypeMap<Aplicacion,ListarAplicacionOutputDto> typeMap = modelMapper.getTypeMap(Aplicacion.class, ListarAplicacionOutputDto.class);
		if (typeMap == null) {
			modelMapper.addMappings(new PropertyMap<Aplicacion,pe.gob.muni.stdbackend.vista.dtos.output.ListarAplicacionOutputDto>() {
				  @Override
				  protected void configure() {
					//map().setIdUnidadOrganica(source.getUnidadOrganica().getIdUnidadOrganica());
					//map().setNombreUnidadOrganica(source.getUnidadOrganica().getNombre());
					  
					  map().setDescripcion( "prueba");
					
				  }
			}); }
			
		}
	
	
	public static String getPropertyStatic(String nombreProperties) {
		Properties properties = new Properties();
		String wadlURI = "";
		try {
			InputStream inputStream = Funciones.class.getClassLoader().getResourceAsStream("messages.properties");
			properties.load(inputStream);
			wadlURI = properties.getProperty(nombreProperties);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return wadlURI;
	}

	public static <D, T> List<D> mapAll(final Collection<T> entityList, Class<D> outCLass) {
		return entityList.stream().map(entity -> map(entity, outCLass)).collect(Collectors.toList());
	}

	public static void validarOperacionConBaseDatos(ModeloBase modeloBase, Class<?> genericType, String nombreMetodo) {
		if (modeloBase == null) {
			throw new CustomException("Ocurrió un error interno.",
					"El modelo no tiene base." + " clase: " + genericType.getName() + " metodo: " + nombreMetodo);
		}
		if (modeloBase.getResultado() == 0) {
			throw new CustomException(modeloBase.getMensaje(),
					modeloBase.getMensaje() + " clase: " + genericType.getName() + " metodo: " + nombreMetodo);
		} else if (modeloBase.getResultado() == -1) {
			if (modeloBase.getMensaje() == null) {
				modeloBase.setMensaje("Ocurrió un error interno.");
			}		
			throw new CustomException("Ocurrió un error interno.",
					modeloBase.getMensaje() + " clase: " + genericType.getName() + " metodo: " + nombreMetodo);
		} else if (modeloBase.getResultado() == -2) {
			if (modeloBase.getMensaje() == null) {
				modeloBase.setMensaje("Ocurrió un error interno.");
			}
			if (modeloBase.getMensajeInterno() == null) {
				modeloBase.setMensajeInterno("Ocurrió un error interno.");
			}
			throw new CustomException(modeloBase.getMensaje(),
					modeloBase.getMensajeInterno() + " clase: " + genericType.getName() + " metodo: " + nombreMetodo);
		}else if (modeloBase.getResultado() == -3) {
			if (modeloBase.getMensaje() == null) {
				modeloBase.setMensaje("Ocurrió un error interno.");
			}
			if (modeloBase.getMensajeInterno() == null) {
				modeloBase.setMensajeInterno("Ocurrió un error interno.");
			}
			throw new SessionDuplicateException(modeloBase.getMensaje(),
					modeloBase.getMensajeInterno() + " clase: " + genericType.getName() + " metodo: " + nombreMetodo);
		}
	}

	public static <S, D> D map(final S source, D destination) {
		
		modelMapper.map(source, destination);
		return destination;
	}

	public static <D, T> D map(final T entity, Class<D> outClass) {
		if (entity == null) {
			return null;
		}
		return modelMapper.map(entity, outClass);
	}

	public static Map<String, Object> getMapFromIoJsonwebtokenClaims(DefaultClaims claims) {
		Map<String, Object> expectedMap = new HashMap<String, Object>();
		for (Entry<String, Object> entry : claims.entrySet()) {
			expectedMap.put(entry.getKey(), entry.getValue());
		}
		return expectedMap;
	}


	private static final List<Integer> VALID_PWD_CHARS = new ArrayList<>();
	static {
		IntStream.rangeClosed(0, 9).forEach(VALID_PWD_CHARS::add); // 0-9
		IntStream.rangeClosed('A', 'Z').forEach(VALID_PWD_CHARS::add); // A-Z
		IntStream.rangeClosed('a', 'z').forEach(VALID_PWD_CHARS::add); // a-z
		IntStream.rangeClosed('!', '*').forEach(VALID_PWD_CHARS::add); // !-*
	}

	public static String generarCodigo() {
		Integer len = 6;
		// ASCII range - alphanumeric (0-9, a-z, A-Z)
		// final String chars =
		// "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		final String chars = "0123456789";

		SecureRandom random = new SecureRandom();
		StringBuilder sb = new StringBuilder();

		// each iteration of loop choose a character randomly from the given ASCII range
		// and append it to StringBuilder instance

		for (int i = 0; i < len; i++) {
			int randomIndex = random.nextInt(chars.length());
			sb.append(chars.charAt(randomIndex));
		}

		// System.out.println("random: " + sb.toString());

		return sb.toString();
	}

	public static String fileExtension(File file) {
		String name = file.getName();
		if (name.lastIndexOf(".") != -1 && name.lastIndexOf(".") != 0)
			return name.substring(name.lastIndexOf(".") + 1);
		else
			return "";
	}

	public static String fileExtension(String file) {
		String name = file;
		if (name.lastIndexOf(".") != -1 && name.lastIndexOf(".") != 0)
			return name.substring(name.lastIndexOf(".") + 1);
		else
			return "";
	}
	
	 public static String getPasswordHash(String password) {
		 String hash = "";
		 MessageDigest messageDigest;
		try {
			messageDigest = MessageDigest.getInstance("SHA-1");
			messageDigest.reset();
			messageDigest.update(password.getBytes("utf8"));
		     hash = String.format("%040x", new BigInteger(1, messageDigest.digest()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	     return hash;
    }
	 
	 

   public static String getContraseniaTemporal() {
	        StringBuilder pass = new StringBuilder();
	        String str = "acbdefghijklmnopqrstuvwxyz0123456789";
	        int sizeGenerator = 8;
	        for (int ii = 0; ii < sizeGenerator; ii++) {
	            SecureRandom random = new SecureRandom(); 
	            byte[] bytes = new byte[20];
	            random.nextBytes(bytes);
	            int posPass = (int) Math.floor(random.nextDouble() * ((str.length() - 1) + 1));
	            pass.append(str.substring(posPass, posPass + 1));
	        }
	        return pass.toString();
	    }
   
   
   public static String obtenerValorToken(JWTTokenProvider jwtTokenProvider, String token, String nombreCampo) {
		HashMap<String, Object> claimw = jwtTokenProvider.obtenerPayload(token);		
		String valor = claimw.get(nombreCampo).toString();
		return valor;
	}

}
