package pe.gob.muni.stdbackend.transversal.excepciones;

public class InvalidReCaptchaException extends RuntimeException {
	private String mensajeInterno;
	private String mensaje;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidReCaptchaException(String message) {
		super(message);
		this.setMensaje(message);
		this.setMensajeInterno(message);
		// TODO Auto-generated constructor stub
	}

	public InvalidReCaptchaException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public InvalidReCaptchaException(String message, Throwable rootCause) {
		super(message, rootCause);
		// TODO Auto-generated constructor stub
	}

	public InvalidReCaptchaException(String mensaje, String mensajeInterno) {
		this.setMensaje(mensaje);
		this.setMensajeInterno(mensajeInterno);
		// TODO Auto-generated constructor stub
	}

	public InvalidReCaptchaException(Throwable rootCause) {
		super(rootCause);
		// TODO Auto-generated constructor stub
	}

	public String getMensajeInterno() {
		return mensajeInterno;
	}

	public void setMensajeInterno(String mensajeInterno) {
		this.mensajeInterno = mensajeInterno;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
}
