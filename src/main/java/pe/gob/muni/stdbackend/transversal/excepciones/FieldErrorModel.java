package pe.gob.muni.stdbackend.transversal.excepciones;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class FieldErrorModel {
	String fieldName;
	String fieldError;
}
