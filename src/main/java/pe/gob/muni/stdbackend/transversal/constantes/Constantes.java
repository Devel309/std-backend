package pe.gob.muni.stdbackend.transversal.constantes;

public class Constantes {
    public static final Integer ESTADO_ACTIVO = 1;
    public static final Integer ESTADO_INACTIVO = 0;
    public static final String S_ESTADO_ACTIVO= "1";
    public static final String S_ESTADO_INACTIVO= "0";

    public static final String URL_SFTP= "sigea";
    public static final String URL_SIGEA_ORDEN_COMPRA_BIEN= "sigea/orden-compra";
    public static final String URL_SIGEA_VERIF_SERV_POE= "sigea/verif_serv_poe";
    public static final String URL_SIGEA_VERIF_BIEN_POE= "sigea/verif_bien_poe";
    public static final String URL_SIGEA_ESTADO_PEDIDO_POE= "sigea/estado_pedido_poe";
    public static final String URL_SIGEA_REEMBOLSO= "sigea/reembolso";

    public static final Integer PEDIDO_ESTADO_RESERVADO = 1;
    public static final Integer PEDIDO_ESTADO_PENDIENTE_VERIF_COMPRA = 10010;
    public static final Integer PEDIDO_ESTADO_PENDIENTE_ELECCION_GANADOR = 10011;
    public static final Integer PEDIDO_ESTADO_PENDIENTE_CERTIFICACION = 2;
    public static final Integer PEDIDO_ESTADO_CERTIFICADO = 3;
    public static final Integer PEDIDO_ESTADO_CERTIFICADO_PARCIALMENTE = 4;
    public static final Integer PEDIDO_ESTADO_PENDIENTE_CONFORMIDAD = 5;
    public static final Integer PEDIDO_ESTADO_CONFORMIDAD = 6;
    public static final Integer PEDIDO_ESTADO_CANCELADO = 7;
    
    public static final Integer AVANCE_PEDIDO_ESTADO_ENPROCESO = 1;
    public static final Integer AVANCE_PEDIDO_ESTADO_CANCELADO = 0;
    public static final Integer AVANCE_PEDIDO_ESTADO_PROCESADO = 2;

    public static final Integer PEDIDO_TIPO_BIEN = 1;
    public static final Integer PEDIDO_TIPO_SERVICIO = 2;

    public static final Integer SP_RESULT_OK = 1;
    

    public static final Integer CREDITO_ANULACION_TIPO_CREDITO = 1;
    public static final Integer CREDITO_ANULACION_TIPO_ANULACION = 2;
    
    public static final Integer CREDITO_ANULACION_ESTADO_PENDIENTE = 1;
    public static final Integer CREDITO_ANULACION_ESTADO_CANCELADO = 2;
    public static final Integer CREDITO_ANULACION_ESTADO_APROBADO = 3;
    
    
    /*VARIABLES OBTENER MARCO PRESUPUESTAL */
    public static final Integer PEDIDO_BIEN_SERVICIO = 1;
    public static final Integer PEDIDO_CAJA_CHICA = 2;
    public static final Integer PEDIDO_REEMBOLSO = 3;
    public static final Integer PEDIDO_VIATICOS = 4;
    public static final Integer PEDIDO_CREDITO_ANULACION = 5;
}
