package pe.gob.muni.stdbackend.transversal.constantes;

public class ConstanteLdap {
	public static final String ERROR_LOGON_FAILURE = "52e";
	public static final String ERROR_ACCOUNT_RESTRICTION = "52f";
	public static final String ERROR_INVALID_LOGON_HOURS = "530";
	public static final String ERROR_INVALID_WORKSTATION = "531";
	public static final String ERROR_PASSWORD_EXPIRED = "532";
	public static final String ERROR_ACCOUNT_DISABLED = "533";
	public static final String ERROR_ACCOUNT_EXPIRED = "701";
	public static final String ERROR_PASSWORD_MUST_CHANGE = "773";
	public static final String ERROR_ACCOUNT_LOCKED_OUT = "775";
	
	
}
