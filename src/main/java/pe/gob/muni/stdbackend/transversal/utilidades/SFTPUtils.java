package pe.gob.muni.stdbackend.transversal.utilidades;


import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Objects;
import java.util.Vector;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;


public class SFTPUtils {
    

	private Logger logger = LoggerFactory.getLogger(SFTPUtils.class);

	private String hostName;
	private String hostPort;
	private String userName;
	private String passWord;
	private String destinationDir;

	private ChannelSftp channelSftp = null;
	private Session session = null;
	private Channel channel = null;

	private int userGroupId = 0;

	private static final String SEPARADOR_ARCHIVO = ".";
 
    private static final String FORMATO_FECHA_HORA = "_yyyyMMddHHmmssSSS";
    private static final String STRING_VACIO = "";

	public SFTPUtils() {

	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public String getHostPort() {
		return hostPort;
	}

	public void setHostPort(String hostPort) {
		this.hostPort = hostPort;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassWord() {
		return passWord;
	}

	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}

	public String getDestinationDir() {
		return destinationDir;
	}

	public void setDestinationDir(String destinationDir) {
		this.destinationDir = destinationDir;
	}

	public int getUserGroupId() {
		return userGroupId;
	}

	public void setUserGroupId(int userGroupId) {
		this.userGroupId = userGroupId;
	}

	private void initChannelSftp() {
		channelSftp = null;
		session = null;
		try {

			JSch jsch = new JSch();
			session = jsch.getSession(userName, hostName, Integer.valueOf(hostPort));
			session.setPassword(passWord);
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);

		} catch (Exception ex) {
			logger.error(ex.getMessage());
		}
	}


	public String uploadFileToFTP(String filename, InputStream fis, boolean enableLog) {
		String result = "";

		initChannelSftp();
		try {

			if (!session.isConnected())
				session.connect();
			channel = session.openChannel("sftp");
			channel.connect();
			channelSftp = (ChannelSftp) channel;
			try {
				channelSftp.cd(destinationDir);
			} catch (SftpException e) {
				channelSftp.mkdir(destinationDir);
				channelSftp.cd(destinationDir);
			}

			channelSftp.put(fis, filename);
			logger.info("Upload successful portfolio file name:" + filename);
			result = String.format("sftp://%s/%s/%s", hostName, destinationDir, filename);

			channelSftp.exit();
			channel.disconnect();
			session.disconnect();
		} catch (Exception ex) {
			if (enableLog)
				logger.error(ex.getMessage());
		}

		return result;
	}

	public String uploadFileToFTP(String desFileName, String srcFilePath, boolean enableLog) {
		String result = "";
		try {
			InputStream fis = new FileInputStream(srcFilePath);
			result = uploadFileToFTP(desFileName, fis, enableLog);
		} catch (Exception ex) {
			if (enableLog)
				logger.error(ex.getMessage());
		}
		return result;
	}

	public boolean checkExist(String fileName) {
		boolean existed = false;

		initChannelSftp();
		try {
			if (!session.isConnected())
				session.connect();
			channel = session.openChannel("sftp");
			channel.connect();
			channelSftp = (ChannelSftp) channel;
			
			if(!destinationDir.isEmpty())
				channelSftp.cd(destinationDir);

			Vector<?> ls = channelSftp.ls(destinationDir);
			if (ls != null) {
				logger.info(fileName);
				for (int i = 0; i < ls.size(); i++) {
					LsEntry entry = (LsEntry) ls.elementAt(i);
					String file_name = entry.getFilename();
					if (!entry.getAttrs().isDir()) {
						if (fileName.toLowerCase().startsWith(file_name)) {
							existed = true;
						}
					}
				}
			}
			channelSftp.exit();
			channel.disconnect();
			session.disconnect();
		} catch (Exception ex) {
			existed = false;
			if (session.isConnected()) {
				session.disconnect();
			}
		}
		return existed;
	}

	public void deleteFile(String fileName) throws Exception {

		initChannelSftp();
		try {
			if (!session.isConnected())
				session.connect();
			channel = session.openChannel("sftp");
			channel.connect();
			channelSftp = (ChannelSftp) channel;
			
			if(!destinationDir.isEmpty())
				channelSftp.cd(destinationDir);
			
			channelSftp.rm(fileName);
			channelSftp.exit();
			channel.disconnect();
			session.disconnect();
		} catch (Exception ex) {
			logger.info(ex.getMessage());
			if (session.isConnected()) {
				session.disconnect();
			}
			throw ex;
		}

	}
 

	public Boolean remoteExtractToZipFile(String fileName, String decpath) throws Exception {
		Boolean result = false;

		initChannelSftp();
		try {

			if (!session.isConnected())
				session.connect();
			channel = session.openChannel("exec");
			ChannelExec channelExec = (ChannelExec) channel;
			String command = "unzip -o " + fileName;

			if (!destinationDir.isEmpty())
				command = "cd " + destinationDir + "; " + command;
			if (!decpath.isEmpty())
				command = " -d " + decpath;

			channelExec.setCommand(command);
			channel.connect();

			while (true) {
				if (channel.isClosed()) {
					System.out.println("exit-status: " + channel.getExitStatus());
					break;
				}
				try {
					Thread.sleep(1000);
				} catch (Exception ee) {
				}
			}

			channel.disconnect();
			session.disconnect();
			result = true;
		} catch (Exception ex) {

			logger.error(ex.getMessage());
			throw ex;
		}

		return result;

	}

	public String saveFile(MultipartFile multipartFile, String path, String nameFile) throws Exception {
		
 
		
		String nombreNuevo = null;

		initChannelSftp();
		InputStream inputStream = null;
        OutputStream outputStream = null;
		
		try {
			if (!session.isConnected())
				session.connect();
			channel = session.openChannel("sftp");
			channel.connect();
			channelSftp = (ChannelSftp) channel;
			
			try {
				channelSftp.cd(destinationDir);
			} catch (SftpException e) {
				channelSftp.mkdir(destinationDir);
				channelSftp.cd(destinationDir);
			}
			
			nombreNuevo = getFileName(nameFile);


			inputStream = new BufferedInputStream(multipartFile.getInputStream());
            outputStream = channelSftp.put(nombreNuevo);

			FileCopyUtils.copy(inputStream, outputStream);


			channelSftp.exit();
			channel.disconnect();
			session.disconnect();
		} catch (Exception ex) {		 
			if (session.isConnected()) {
				session.disconnect();
			}
			throw ex;
		}

		return nombreNuevo;
	}

	private String getFileName(String fullFileName) {
        SimpleDateFormat formatter = new SimpleDateFormat(FORMATO_FECHA_HORA);
        Date date = new Date();
        if (Objects.nonNull(fullFileName)) {
            String newFileName = FilenameUtils.getBaseName(fullFileName)
                    .concat(formatter.format(date))
                    .concat(SEPARADOR_ARCHIVO)
                    .concat(FilenameUtils.getExtension(fullFileName));
            return newFileName;
        }
        return STRING_VACIO;
    }

	public HashMap<String,String> readFileTXT(String filename, boolean enableLog) throws Exception {

		HashMap<String,String>  resultado = new HashMap<>();
		
		String content = null;
		String filenameSucess = null;
		
		initChannelSftp();

		try {

			if (!session.isConnected())
				session.connect();
			channel = session.openChannel("sftp");
			channel.connect();
			channelSftp = (ChannelSftp) channel;

			if (!destinationDir.isEmpty())
				channelSftp.cd(destinationDir);

			String file_name = null;

			Vector<LsEntry> ls = channelSftp.ls(filename);
			ls.sort(Comparator.comparing((LsEntry file) -> file.getFilename()));
				      
			if (ls != null) {
				// for (int i = 0; i < ls.size(); i++) {
				for (int i = ls.size() - 1; i >= 0; i--) {
					LsEntry entry = (LsEntry) ls.elementAt(i);
					if (!entry.getAttrs().isDir()) {
						file_name = entry.getFilename();
						content = readInputStreamToString(channelSftp.get(file_name));
						break;
					}
				}
			}

			if (file_name != null) {
				filenameSucess = file_name;
				logger.info("Get successful portfolio file name:" + file_name);
			} else
				logger.info("No exists file name:" + filename);

			channelSftp.exit();
			channel.disconnect();
			session.disconnect();
		} catch (Exception ex) {
			if (enableLog)
				logger.error(ex.getMessage());

			throw ex;
		}

		resultado.put("filename", filenameSucess);
		resultado.put("content", content);
		
		return resultado;
	}

	private String readInputStreamToString(InputStream input) throws Exception {

		String content = null;
		try {
			InputStreamReader isReader = new InputStreamReader(input);
			BufferedReader reader = new BufferedReader(isReader);
			StringBuffer sb = new StringBuffer();
			String str;
			while ((str = reader.readLine()) != null) {
				sb.append(str + System.lineSeparator());
			}
			content = sb.toString();
		} catch (Exception e) {
			logger.error(e.getMessage());
			content = null;
		}
		return content;
	}



	public void saveFile(File file, String path) throws Exception {

		initChannelSftp();
		InputStream inputStream = null;
        OutputStream outputStream = null;
		
		try {
			if (!session.isConnected())
				session.connect();
			channel = session.openChannel("sftp");
			channel.connect();
			channelSftp = (ChannelSftp) channel;
			
			String nombreNuevo = getFileName(file.getName());


			inputStream = new BufferedInputStream(new FileInputStream(file));
            outputStream = channelSftp.put(path+"/"+nombreNuevo);

			FileCopyUtils.copy(inputStream, outputStream);


			channelSftp.exit();
			channel.disconnect();
			session.disconnect();
		} catch (Exception ex) {		 
			if (session.isConnected()) {
				session.disconnect();
			}
			throw ex;
		}
	}
	
	
	public String getFileBase64(String path) throws Exception {

		String encoded = null;
		initChannelSftp();
		InputStream inputStream = null;
        OutputStream outputStream = null;
		
		try {
			if (!session.isConnected())
				session.connect();
			channel = session.openChannel("sftp");
			channel.connect();
			channelSftp = (ChannelSftp) channel;
			


			InputStream stream = channelSftp.get(path);
			 
			byte[] bytes = IOUtils.toByteArray(stream);
			 

		    encoded = Base64.getEncoder().encodeToString(bytes);

		    
			channelSftp.exit();
			channel.disconnect();
			session.disconnect();
		} catch (Exception ex) {		 
			if (session.isConnected()) {
				session.disconnect();
			}
			throw ex;
		}
		
		return encoded;
	}
}
