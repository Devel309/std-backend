package pe.gob.muni.stdbackend.transversal.properties;

import java.util.Properties;
import javax.annotation.Resource;
import org.springframework.stereotype.Component;
import lombok.extern.log4j.Log4j2;

@Component
@Log4j2
public class SeguridadValidarProperties {

	@Resource(mappedName = "resource/registro_properties")
	private Properties properties;

	private Boolean token;

	public Boolean getToken() {
		return Boolean.parseBoolean(this.getProperty("seguridad.validar.token"));
	}

	private String getProperty(String key) {
		Object value = properties.get(key);
		if (value != null) {
			return value.toString();
		}
		return null;
	}
}
