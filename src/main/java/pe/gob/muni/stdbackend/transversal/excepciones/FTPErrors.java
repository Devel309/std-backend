package pe.gob.muni.stdbackend.transversal.excepciones;

public class FTPErrors extends Exception {

	private static final long serialVersionUID = 1L;
	private ErrorMessage errorMessage;

    public FTPErrors(ErrorMessage errorMessage) {
        super(errorMessage.getErrormessage());
    }

    public ErrorMessage getErrorMessage() {
        return errorMessage;
    }
}
