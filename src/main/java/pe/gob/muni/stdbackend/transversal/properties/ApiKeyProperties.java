package pe.gob.muni.stdbackend.transversal.properties;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.util.Properties;

@Component
@Log4j2
public class ApiKeyProperties {

	@Resource(mappedName = "resource/registro_properties")
	private Properties properties;

	private String header;
	private String prefix;

	public String getHeader() {
		return this.getProperty("apikey.header");
	}

	public String getPrefix() {
		return this.getProperty("apikey.header.prefix").trim();
	}

	private String getProperty(String key) {
		Object value = properties.get(key);
		if (value != null) {
			return value.toString();
		}
		return null;
	}
}
