package pe.gob.muni.stdbackend.transversal.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import pe.gob.muni.stdbackend.transversal.properties.CrossOriginProperties;

/**
 * @author glennlq
 * @created 12/22/21
 */
//@Configuration
//@EnableWebMvc
//public class WebConfig implements WebMvcConfigurer {
//
//    @Autowired
//    CrossOriginProperties crossOriginProperties;
//
//    @Override
//    public void addCorsMappings(CorsRegistry registry) {
//        registry.addMapping("/**")
//        .allowedMethods("GET","POST","DELETE","PUT","OPTIONS")
//        .allowCredentials(true)
//        .allowedOrigins(crossOriginProperties.getAllowedOrigins2());
//    }
//}
