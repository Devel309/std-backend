package pe.gob.muni.stdbackend.transversal.utilidades;

import pe.gob.muni.stdbackend.transversal.constantes.ConstanteLdap;

public class FuncionesLdap {

	public static String getErrorMessage(String codigo) {
		String message = "error";
		switch (codigo) {
		case ConstanteLdap.ERROR_LOGON_FAILURE:
			message = Funciones.getPropertyStatic("ldap.error.login");
			break;
		case ConstanteLdap.ERROR_ACCOUNT_RESTRICTION:
			message = Funciones.getPropertyStatic("ldap.error.restriccion.cuenta");
			break;
		case ConstanteLdap.ERROR_INVALID_LOGON_HOURS:
			message = Funciones.getPropertyStatic("ldap.error.hora.limite");
			break;
		case ConstanteLdap.ERROR_INVALID_WORKSTATION:
			message = Funciones.getPropertyStatic("ldap.error.estacion.trabajo");
			break;
		case ConstanteLdap.ERROR_PASSWORD_EXPIRED:
			message = Funciones.getPropertyStatic("ldap.error.password.expirado");
			break;
		case ConstanteLdap.ERROR_ACCOUNT_DISABLED:
			message = Funciones.getPropertyStatic("ldap.error.cuenta.deshabilitada");
			break;
		case ConstanteLdap.ERROR_ACCOUNT_EXPIRED:
			message = Funciones.getPropertyStatic("ldap.error.cuenta.expirada");
			break;
		case ConstanteLdap.ERROR_PASSWORD_MUST_CHANGE:
			message = Funciones.getPropertyStatic("ldap.error.password.cambiar");
			break;
		case ConstanteLdap.ERROR_ACCOUNT_LOCKED_OUT:
			message = Funciones.getPropertyStatic("ldap.error.cuenta.fuera");
			break;

		}
		return message;
	}
}
