package pe.gob.muni.stdbackend.transversal.excepciones;

public class ReCaptchaInvalidException extends RuntimeException {
	private String mensajeInterno;
	private String mensaje;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ReCaptchaInvalidException(String message) {
		super(message);
		this.setMensaje(message);
		this.setMensajeInterno(message);
		// TODO Auto-generated constructor stub
	}

	public ReCaptchaInvalidException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ReCaptchaInvalidException(String message, Throwable rootCause) {
		super(message, rootCause);
		// TODO Auto-generated constructor stub
	}

	public ReCaptchaInvalidException(String mensaje, String mensajeInterno) {
		this.setMensaje(mensaje);
		this.setMensajeInterno(mensajeInterno);
		// TODO Auto-generated constructor stub
	}

	public ReCaptchaInvalidException(Throwable rootCause) {
		super(rootCause);
		// TODO Auto-generated constructor stub
	}

	public String getMensajeInterno() {
		return mensajeInterno;
	}

	public void setMensajeInterno(String mensajeInterno) {
		this.mensajeInterno = mensajeInterno;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
}
