package pe.gob.muni.stdbackend.transversal.properties;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

@Component
@Log4j2
public class CrossOriginProperties {

	@Resource(mappedName = "resource/registro_properties")
	private Properties properties;

	public boolean getOriginsHabilitado(){
		return Boolean.parseBoolean(this.getProperty("crossorigin.habilitado"));
	}

	public List<String> getAllowedOrigins() {
		String urls = this.getProperty("crossorigin.urls");
		System.err.println("crossorigin.urls:"+ urls);
		return Arrays.asList(urls.split(","));
	}

	public String[] getAllowedOrigins2() {
		String urls = this.getProperty("crossorigin.urls");
		System.err.println("crossorigin.urls:"+ urls);
		return urls.split(",");
	}

	private String getProperty(String key) {
		Object value = properties.get(key);
		if (value != null) {
			return value.toString();
		}
		return null;
	}
}
