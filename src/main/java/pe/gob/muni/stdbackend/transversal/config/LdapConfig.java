package pe.gob.muni.stdbackend.transversal.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;

import lombok.extern.slf4j.Slf4j;
import pe.gob.muni.stdbackend.transversal.properties.LdapProperties;

@Slf4j
@Configuration
public class LdapConfig {
	@Autowired
	LdapProperties ldapProperties;

	@Bean
	public LdapContextSource contextSource() {
		LdapContextSource contextSource = new LdapContextSource();
		contextSource.setUrl(ldapProperties.getUrl());
		contextSource.setBase(ldapProperties.getBase());
		contextSource.setUserDn(ldapProperties.getPrincipal());
		contextSource.setPassword(ldapProperties.getPassword());
		contextSource.setPooled(false);

		return contextSource;
	}

	@Bean
	public LdapTemplate ldapTemplate() {
		return new LdapTemplate(contextSource());
	}
}
