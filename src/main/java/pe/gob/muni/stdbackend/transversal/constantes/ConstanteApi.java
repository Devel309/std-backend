package pe.gob.muni.stdbackend.transversal.constantes;

 
public class ConstanteApi {
	public static final String VERSION_API = "/v1";
	public static final String[] APIS_LIBRES = new String[] { "/csrf",
			ConstanteApi.VERSION_API + "/api/usuario/acceder-sistema",
			ConstanteApi.VERSION_API + "/api/usuario/registrationCaptchaV3",
			ConstanteApi.VERSION_API + "/api/usuario/test", 
			ConstanteApi.VERSION_API + "/api/seguridad/refreshtoken",
			ConstanteApi.VERSION_API + "/api/usuario/login", 
			ConstanteApi.VERSION_API + "/api/usuario",
			ConstanteApi.VERSION_API + "/api/usuario/login",
			ConstanteApi.VERSION_API + "/api/usuario/generar-sesion-activa",
			ConstanteApi.VERSION_API + "/api/usuario/validar-sesion",
			ConstanteApi.VERSION_API + "/api/restfull",
			ConstanteApi.VERSION_API + "/api/recursogenerico/apigoogle"
			
			// , ConstanteApi.VERSION_API + "/api/proveedor/generar-reporte-proveedor"
			//, ConstanteApi.VERSION_API + "/", "/" 
			};

	public static final String[] URL_WEB_LIBRES = new String[] { 
			"/v2/api-docs", 
			"/v3/api-docs",
			"/swagger-resources/**",
			"/swagger-ui/**",
			"/swagger-resources/configuration/security",
			"/swagger-resources/configuration/ui",
			"/swagger-ui/index.html**", "/webjars/**", "/actuator/health",
			"/webjars",
			"/swagger-resources",
			"/configuration/ui", "/swagger-resources/**", "/configuration/security", "/swagger-ui.html","/swagger-ui/index.html",
			"/webjars/**" };
	
	
	public static final Integer TIPO_DOCUMENTO_DNI = 1;
}
