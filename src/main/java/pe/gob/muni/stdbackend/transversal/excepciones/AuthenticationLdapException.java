package pe.gob.muni.stdbackend.transversal.excepciones;

import pe.gob.muni.stdbackend.transversal.utilidades.FuncionesLdap;

public class AuthenticationLdapException extends RuntimeException {
	private String mensajeInterno;
	private String mensaje;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AuthenticationLdapException(String message) {
		super(message);
		this.setMensaje(message);
		// TODO Auto-generated constructor stub
	}

	public AuthenticationLdapException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AuthenticationLdapException(String message, Throwable rootCause) {
		super(message, rootCause);
		// TODO Auto-generated constructor stub
	}

	public AuthenticationLdapException(String mensaje, String mensajeInterno) {
		
	
		String[] informacion = mensajeInterno.split(",");
		String codError = "";
		for (String inf : informacion) {
			if (inf.contains("data")) {
				codError = inf.replace("data", "").trim();
			}
		}
		String mensajeError = FuncionesLdap.getErrorMessage(codError);
		this.setMensaje(mensajeError);
		this.setMensajeInterno(mensajeInterno);
		// TODO Auto-generated constructor stub
	}

	public AuthenticationLdapException(Throwable rootCause) {
		super(rootCause);
		// TODO Auto-generated constructor stub
	}

	public String getMensajeInterno() {
		return mensajeInterno;
	}

	public void setMensajeInterno(String mensajeInterno) {
		this.mensajeInterno = mensajeInterno;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
}
