package pe.gob.muni.stdbackend.transversal.excepciones;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.naming.AuthenticationException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import io.jsonwebtoken.ExpiredJwtException;
import pe.gob.muni.stdbackend.transversal.utilidades.FuncionesLdap;

@Order()
@RestControllerAdvice
public class HandlerException {
	private static final Logger logger = Logger.getLogger(HandlerException.class.getCanonicalName());

	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public @ResponseBody ExceptionResponse methodArgumentNotValidException(
			final MethodArgumentNotValidException exception, final HttpServletRequest request) {

		// get spring errors
		BindingResult result = exception.getBindingResult();
		List<FieldError> fieldErrors = result.getFieldErrors();

		List<FieldErrorModel> errors = new ArrayList<>();

		// Locale currentLocale = LocaleContextHolder.getLocale();
		for (FieldError fieldError : fieldErrors) {
			String errorMessage = fieldError.getDefaultMessage();
			String campo = fieldError.getField();
			FieldErrorModel error = new FieldErrorModel(campo, errorMessage);
			errors.add(error);
		}
		StackTraceElement l = exception.getStackTrace()[0];
		ExceptionResponse error = new ExceptionResponse();
		error.mensaje = "Error de campos";
		error.mensajeInteno = exception.getMessage();
		error.errorsField = errors;
		error.requestedURI = request.getRequestURI();
		error.timestamp = LocalDateTime.now();
		error.estado = 0;
		error.resultado = 0;
		error.metodo = l.getMethodName();
		error.clase = l.getClassName();
		error.lineaCodigoError = "Linea: " + l.getLineNumber();

		StringWriter sw = new StringWriter();
		exception.printStackTrace(new PrintWriter(sw));
		String exceptionAsString = sw.toString();
		logger.info(exceptionAsString);
		return error;
	}

	@ExceptionHandler(NullPointerException.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public @ResponseBody ExceptionResponse nullPointerException(final NullPointerException exception,
			final HttpServletRequest request) {

		ExceptionResponse error = new ExceptionResponse();
		StackTraceElement l = exception.getStackTrace()[0];

		error.mensaje = "Existe un campo con valor nulo.";
		error.mensajeInteno = exception.getMessage();
		error.requestedURI = request.getRequestURI();
		error.timestamp = LocalDateTime.now();
		error.estado = -1;
		error.resultado = -1;
		error.metodo = l.getMethodName();
		error.clase = l.getClassName();
		error.lineaCodigoError = "Linea: " + l.getLineNumber();

		StringWriter sw = new StringWriter();
		exception.printStackTrace(new PrintWriter(sw));
		String exceptionAsString = sw.toString();
		logger.info(exceptionAsString);

		return error;
	}

	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public @ResponseBody ExceptionResponse authenticationException(final AuthenticationException exception,
			final HttpServletRequest request) {

		ExceptionResponse error = new ExceptionResponse();
		StackTraceElement l = exception.getStackTrace()[0];

		String[] informacion = exception.getMessage().split(",");
		String codError = "";
		for (String inf : informacion) {
			if (inf.contains("data")) {
				codError = inf.replace("data", "").trim();
			}
		}

		String mensaje = FuncionesLdap.getErrorMessage(codError);

		error.mensaje = mensaje;
		error.mensajeInteno = exception.getMessage();
		error.requestedURI = request.getRequestURI();
		error.timestamp = LocalDateTime.now();
		error.estado = -1;
		error.resultado = -1;
		error.metodo = l.getMethodName();
		error.clase = l.getClassName();
		error.lineaCodigoError = "Linea: " + l.getLineNumber();

		StringWriter sw = new StringWriter();
		exception.printStackTrace(new PrintWriter(sw));
		String exceptionAsString = sw.toString();
		logger.info(exceptionAsString);

		return error;
	}
	
	@ExceptionHandler(AuthenticationLdapException.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public @ResponseBody ExceptionResponse authenticationLdapException(
			final AuthenticationLdapException exception,
			final HttpServletRequest request) {

		ExceptionResponse error = new ExceptionResponse();
		StackTraceElement l = exception.getStackTrace()[0];

		error.mensaje = exception.getMensaje();
		error.mensajeInteno = exception.getMensajeInterno();
		error.requestedURI = request.getRequestURI();
		error.timestamp = LocalDateTime.now();
		error.estado = -1;
		error.resultado = -1;
		error.metodo = l.getMethodName();
		error.clase = l.getClassName();
		error.lineaCodigoError = "Linea: " + l.getLineNumber();

		StringWriter sw = new StringWriter();
		exception.printStackTrace(new PrintWriter(sw));
		String exceptionAsString = sw.toString();
		logger.info(exceptionAsString);

		return error;
	}
	
	@ExceptionHandler(ExpiredJwtException.class)
	@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
	public @ResponseBody ExceptionResponse tokenExpiredException(final ExpiredJwtException exception,
			final HttpServletRequest request) {

		ExceptionResponse error = new ExceptionResponse();
		StackTraceElement l = exception.getStackTrace()[0];

		error.mensaje = exception.getMessage();
		error.mensajeInteno = exception.getMessage();
		error.requestedURI = request.getRequestURI();
		error.timestamp = LocalDateTime.now();
		error.estado = -1;
		error.resultado = -1;
		error.metodo = l.getMethodName();
		error.clase = l.getClassName();
		error.lineaCodigoError = "Linea: " + l.getLineNumber();

		StringWriter sw = new StringWriter();
		exception.printStackTrace(new PrintWriter(sw));
		String exceptionAsString = sw.toString();
		logger.info(exceptionAsString);

		return error;
	}

	@ExceptionHandler(CustomException.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public @ResponseBody ExceptionResponse customException(final CustomException exception,
			final HttpServletRequest request) {

		ExceptionResponse error = new ExceptionResponse();
		StackTraceElement l = exception.getStackTrace()[0];

		error.mensaje = exception.getMensaje();
		error.mensajeInteno = exception.getMensajeInterno();
		error.requestedURI = request.getRequestURI();
		error.timestamp = LocalDateTime.now();
		error.estado = -1;
		error.resultado = -1;
		error.metodo = l.getMethodName();
		error.clase = l.getClassName();
		error.lineaCodigoError = "Linea: " + l.getLineNumber();

		StringWriter sw = new StringWriter();
		exception.printStackTrace(new PrintWriter(sw));
		String exceptionAsString = sw.toString();
		logger.info(exceptionAsString);

		return error;
	}

	@ExceptionHandler(InvalidReCaptchaException.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public @ResponseBody ExceptionResponse invalidReCaptchaException(final InvalidReCaptchaException exception,
			final HttpServletRequest request) {

		ExceptionResponse error = new ExceptionResponse();
		StackTraceElement l = exception.getStackTrace()[0];

		error.mensaje = exception.getMensaje();
		error.mensajeInteno = exception.getMensajeInterno();
		error.requestedURI = request.getRequestURI();
		error.timestamp = LocalDateTime.now();
		error.estado = -1;
		error.resultado = -1;
		error.metodo = l.getMethodName();
		error.clase = l.getClassName();
		error.lineaCodigoError = "Linea: " + l.getLineNumber();

		StringWriter sw = new StringWriter();
		exception.printStackTrace(new PrintWriter(sw));
		String exceptionAsString = sw.toString();
		logger.info(exceptionAsString);

		return error;
	}

	@ExceptionHandler(ReCaptchaInvalidException.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public @ResponseBody ExceptionResponse reCaptchaInvalidException(final ReCaptchaInvalidException exception,
			final HttpServletRequest request) {

		ExceptionResponse error = new ExceptionResponse();
		StackTraceElement l = exception.getStackTrace()[0];

		error.mensaje = exception.getMensaje();
		error.mensajeInteno = exception.getMensajeInterno();
		error.requestedURI = request.getRequestURI();
		error.timestamp = LocalDateTime.now();
		error.estado = -1;
		error.resultado = -1;
		error.metodo = l.getMethodName();
		error.clase = l.getClassName();
		error.lineaCodigoError = "Linea: " + l.getLineNumber();

		StringWriter sw = new StringWriter();
		exception.printStackTrace(new PrintWriter(sw));
		String exceptionAsString = sw.toString();
		logger.info(exceptionAsString);

		return error;
	}

	@ExceptionHandler(ReCaptchaUnavailableException.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public @ResponseBody ExceptionResponse reCaptchaUnavailableException(final ReCaptchaUnavailableException exception,
			final HttpServletRequest request) {

		ExceptionResponse error = new ExceptionResponse();
		StackTraceElement l = exception.getStackTrace()[0];

		error.mensaje = exception.getMensaje();
		error.mensajeInteno = exception.getMensajeInterno();
		error.requestedURI = request.getRequestURI();
		error.timestamp = LocalDateTime.now();
		error.estado = -1;
		error.resultado = -1;
		error.metodo = l.getMethodName();
		error.clase = l.getClassName();
		error.lineaCodigoError = "Linea: " + l.getLineNumber();

		StringWriter sw = new StringWriter();
		exception.printStackTrace(new PrintWriter(sw));
		String exceptionAsString = sw.toString();
		logger.info(exceptionAsString);

		return error;
	}

	@ExceptionHandler(NotAuthorizedException.class)
	@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
	public @ResponseBody ExceptionResponse notAuthorizedException(final NotAuthorizedException exception,
			final HttpServletRequest request) {

		ExceptionResponse error = new ExceptionResponse();
		StackTraceElement l = exception.getStackTrace()[0];

		error.mensaje = exception.getMensaje();
		error.mensajeInteno = exception.getMensajeInterno();
		error.requestedURI = request.getRequestURI();
		error.timestamp = LocalDateTime.now();
		error.estado = -1;
		error.resultado = -1;
		error.metodo = l.getMethodName();
		error.clase = l.getClassName();
		error.lineaCodigoError = "Linea: " + l.getLineNumber();

		StringWriter sw = new StringWriter();
		exception.printStackTrace(new PrintWriter(sw));
		String exceptionAsString = sw.toString();
		logger.info(exceptionAsString);

		return error;
	}
	
	@ExceptionHandler(SessionDuplicateException.class)
	@ResponseStatus(value = HttpStatus.MULTI_STATUS)
	public @ResponseBody ExceptionResponse sessionDuplicateException(final SessionDuplicateException exception,
			final HttpServletRequest request) {

		ExceptionResponse error = new ExceptionResponse();
		StackTraceElement l = exception.getStackTrace()[0];

		error.mensaje = exception.getMensaje();
		error.mensajeInteno = exception.getMensajeInterno();
		error.requestedURI = request.getRequestURI();
		error.timestamp = LocalDateTime.now();
		error.estado = -1;
		error.resultado = -3;
		error.metodo = l.getMethodName();
		error.clase = l.getClassName();
		error.lineaCodigoError = "Linea: " + l.getLineNumber();

		StringWriter sw = new StringWriter();
		exception.printStackTrace(new PrintWriter(sw));
		String exceptionAsString = sw.toString();
		logger.info(exceptionAsString);

		return error;
	}

	@ExceptionHandler(ServletException.class)
	@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
	public @ResponseBody ExceptionResponse servletException(final ServletException exception,
			final HttpServletRequest request) {

		ExceptionResponse error = new ExceptionResponse();
		// StackTraceElement l = exception.getStackTrace()[0];
		error.mensaje = exception.getMessage();
		error.mensajeInteno = exception.getMessage();
		error.requestedURI = request.getRequestURI();
		error.timestamp = LocalDateTime.now();
		error.estado = -1;
		error.resultado = -1;
		// error.metodo = l.getMethodName();
		// error.clase = l.getClassName();
		// error.lineaCodigoError = "Linea: " + l.getLineNumber();

		StringWriter sw = new StringWriter();
		exception.printStackTrace(new PrintWriter(sw));
		String exceptionAsString = sw.toString();
		logger.info(exceptionAsString);

		return error;
	}

	@ExceptionHandler(java.io.IOException.class)
	@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
	public @ResponseBody ExceptionResponse IOException(final java.io.IOException exception,
			final HttpServletRequest request) {

		ExceptionResponse error = new ExceptionResponse();
		StackTraceElement l = exception.getStackTrace()[0];

		error.mensaje = exception.getMessage();
		error.mensajeInteno = exception.getMessage();
		error.requestedURI = request.getRequestURI();
		error.timestamp = LocalDateTime.now();
		error.estado = -1;
		error.resultado = -1;
		error.metodo = l.getMethodName();
		error.clase = l.getClassName();
		error.lineaCodigoError = "Linea: " + l.getLineNumber();

		StringWriter sw = new StringWriter();
		exception.printStackTrace(new PrintWriter(sw));
		String exceptionAsString = sw.toString();
		logger.info(exceptionAsString);

		return error;
	}

	@ExceptionHandler(Exception.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public @ResponseBody ExceptionResponse handleException(final Exception exception,
			final HttpServletRequest request) {

		ExceptionResponse error = new ExceptionResponse();
		StackTraceElement l = exception.getStackTrace()[0];
		error.mensaje = "Ocurrió un error interno.";
		error.mensajeInteno = exception.getMessage();
		error.requestedURI = request.getRequestURI();
		error.timestamp = LocalDateTime.now();
		error.estado = -1;
		error.resultado = -1;
		error.metodo = l.getMethodName();
		error.clase = l.getClassName();
		error.lineaCodigoError = "Linea: " + l.getLineNumber();

		StringWriter sw = new StringWriter();
		exception.printStackTrace(new PrintWriter(sw));
		String exceptionAsString = sw.toString();
		logger.info(exceptionAsString);

		// exception.getStackTrace();
		return error;
	}
	
	@ExceptionHandler(CustomBussinessException.class)
	@ResponseStatus(value = HttpStatus.METHOD_FAILURE)
	public @ResponseBody ExceptionResponse customBussinessException(final CustomBussinessException exception,
			final HttpServletRequest request) {

		ExceptionResponse error = new ExceptionResponse();
		StackTraceElement l = exception.getStackTrace()[0];

		error.mensaje = exception.getMensaje();
		error.mensajeInteno = exception.getMensajeInterno();
		error.requestedURI = request.getRequestURI();
		error.timestamp = LocalDateTime.now();
		error.estado = -1;
		error.resultado = -1;
		error.metodo = l.getMethodName();
		error.clase = l.getClassName();
		error.lineaCodigoError = "Linea: " + l.getLineNumber();

		StringWriter sw = new StringWriter();
		exception.printStackTrace(new PrintWriter(sw));
		String exceptionAsString = sw.toString();
		logger.info(exceptionAsString);

		return error;
	}
}
