package pe.gob.muni.stdbackend.vista.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import pe.gob.muni.stdbackend.negocio.servicios.interfaces.UsuarioService;


@RestController
@RequestMapping("/apiUsuario")
@CrossOrigin(origins = "*")
public class UsuarioController {
	
	@Autowired
	UsuarioService usuarioService;

	@GetMapping("/listUsuario")
	public ResponseEntity<?> listUsuario(@RequestParam("usuario") String usuario) {
		try {
			System.out.println("H "+usuario);
			HashMap<Object, Object> response = new HashMap<>();
			response.put("usuario", usuario);
			usuarioService.listUsuario(response);

			Map<String, Object> res = new HashMap();
			res.put("result", response.get("respuesta"));
			return new ResponseEntity<>(res, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
}
