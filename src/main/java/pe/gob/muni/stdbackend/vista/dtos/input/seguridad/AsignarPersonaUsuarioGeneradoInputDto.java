package pe.gob.muni.stdbackend.vista.dtos.input.seguridad;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AsignarPersonaUsuarioGeneradoInputDto {
	
	
	@NotNull(message = "El campo id de usuario no puede ser nulo")
	private Integer idUsuario;
	@NotBlank(message = "El campo nombres no puede ser nulo ni estar en blanco")
	@NotNull(message = "El campo nombres no puede ser nulo")
	private String nombres;
	private String apellidoPaterno;
	private String apellidoMaterno;
	@NotBlank(message = "El campo numeroDocumento no puede ser nulo ni estar en blanco")
	@NotNull(message = "El campo numeroDocumento no puede ser nulo")
	private String numeroDocumento;
	@NotNull(message = "El campo tipoDocumento no puede ser nulo")
	private Integer tipoDocumento;
	@NotBlank(message = "El campo correo no puede ser nulo ni estar en blanco")
	@NotNull(message = "El campo correo no puede ser nulo")
	private String correo;
	@NotBlank(message = "El campo clave no puede ser nulo ni estar en blanco")
	@NotNull(message = "El campo clave no puede ser nulo")
	private String clave;
	
		
	
	

}