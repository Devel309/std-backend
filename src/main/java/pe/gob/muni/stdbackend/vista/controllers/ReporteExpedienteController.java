package pe.gob.muni.stdbackend.vista.controllers;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.gob.muni.stdbackend.negocio.modelos.BuscarExpedienteDto;
import pe.gob.muni.stdbackend.negocio.servicios.interfaces.ReporteExpedienteService;

@RestController
@RequestMapping("/apiReporteExp")
@CrossOrigin(origins = "*")
public class ReporteExpedienteController {

	@Autowired
	ReporteExpedienteService reporteExpedienteService;
	
	
	@PostMapping("/listReporteExp")
	public ResponseEntity<?> listReporteExp(@RequestBody BuscarExpedienteDto buscarExpedienteDto)
		{
			try {
				BuscarExpedienteDto  param = new BuscarExpedienteDto();
				param.setIdTipoExp(buscarExpedienteDto.getIdTipoExp()>0?buscarExpedienteDto.getIdTipoExp():null);
				param.setCorrelativo(buscarExpedienteDto.getCorrelativo()!=""?buscarExpedienteDto.getCorrelativo():"");
				param.setDni(buscarExpedienteDto.getDni()!=""?buscarExpedienteDto.getDni():"");
				param.setFechaInicio(buscarExpedienteDto.getFechaInicio()!=""?buscarExpedienteDto.getFechaInicio():"");
				param.setFechaFin(buscarExpedienteDto.getFechaFin()!=""?buscarExpedienteDto.getFechaFin():"");
				param.setArea(buscarExpedienteDto.getArea());
				param.setIdArea(buscarExpedienteDto.getIdArea());
				System.out.println("param: "+param);
				HashMap<String, Object> listResponse =  reporteExpedienteService.listReporte(param);
				System.out.println("listResponse: "+listResponse);
				return new ResponseEntity<>(listResponse, HttpStatus.OK);
			}catch (Exception e) {
				e.printStackTrace();
				return new ResponseEntity<>(e.getMessage(),HttpStatus.BAD_REQUEST);
			}
		}
}
