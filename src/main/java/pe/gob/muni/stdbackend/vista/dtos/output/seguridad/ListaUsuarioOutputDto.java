package pe.gob.muni.stdbackend.vista.dtos.output.seguridad;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ListaUsuarioOutputDto {
	private Integer idAplicacionUsuario;
	private Integer tipoDocumento;
	private String numeroDocumento;
	private String nombres;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String estado;
	private String usuario;
	private Integer personaAsignada;
	private String claveTemporal;
	private String correo;
}
