package pe.gob.muni.stdbackend.vista.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import pe.gob.muni.stdbackend.negocio.modelos.BuscarExpedienteDto;
import pe.gob.muni.stdbackend.negocio.modelos.DerivarExpedienteDto;
import pe.gob.muni.stdbackend.negocio.modelos.ExpedienteDto;
import pe.gob.muni.stdbackend.negocio.modelos.ListExpedienteDto;
import pe.gob.muni.stdbackend.negocio.modelos.ListSeguimientoExternoDto;
import pe.gob.muni.stdbackend.negocio.modelos.ListVerDetalleExpedienteDto;
import pe.gob.muni.stdbackend.negocio.servicios.interfaces.ExpedienteService;


@RestController
@RequestMapping("/apiExpediente")
@CrossOrigin(origins = "*")
public class ExpedienteController {

	@Autowired
	ExpedienteService expedienteService;
	
	@PostMapping("/insertExpediente")
	public ResponseEntity<?> insertPersona(@RequestBody ExpedienteDto expedienteDto){
		System.out.println("controller");
		try {
			HashMap<Object, Object> response = new HashMap<>();
   			response.put("tipo", expedienteDto.getTipo());
   			response.put("asuntoExp", expedienteDto.getAsuntoExp());
   			response.put("contenidoExp", expedienteDto.getContenidoExp());
   			response.put("descripcionPdf", expedienteDto.getDescripcionPdf());
            response.put("pdfExp", expedienteDto.getPdfExp());
            response.put("folio", expedienteDto.getFolio());
    		response.put("descripcionAnexo", expedienteDto.getDescripcionAnexo());
   			response.put("pdfAnexo", expedienteDto.getPdfAnexo());
   			response.put("idUsuario", expedienteDto.getIdUsuario());
            response.put("idEstadoExp", expedienteDto.getIdEstadoExp());
            response.put("idTipoExp", expedienteDto.getIdTipoExp());
            response.put("idPersona", expedienteDto.getIdPersona());
   			response.put("carnetExt", expedienteDto.getCarnetExt());
   			response.put("celular", expedienteDto.getCelular());
            response.put("correo", expedienteDto.getCorreo());
            response.put("descripcion", expedienteDto.getDescripcion());
            response.put("result", "");
            expedienteService.createExpediente(response);
			System.out.println("[insertProyecto respuesta]  "+response.get("respuesta"));
			Map<String, Object> res= new HashMap();
			res.put("result", response.get("respuesta"));
			return new ResponseEntity<>(res, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(),HttpStatus.BAD_REQUEST);
		}
	}
	 @PostMapping("/listExpediente")
		public ResponseEntity<?> listExpediente(@RequestBody BuscarExpedienteDto buscarExpedienteDto)
			{
				try {
					BuscarExpedienteDto  param = new BuscarExpedienteDto();
					param.setIdTipoExp(buscarExpedienteDto.getIdTipoExp()>0?buscarExpedienteDto.getIdTipoExp():null);
					param.setCorrelativo(buscarExpedienteDto.getCorrelativo()!=""?buscarExpedienteDto.getCorrelativo():"");
					param.setDni(buscarExpedienteDto.getDni()!=""?buscarExpedienteDto.getDni():"");
					param.setFechaInicio(buscarExpedienteDto.getFechaInicio()!=""?buscarExpedienteDto.getFechaInicio():"");
					param.setFechaFin(buscarExpedienteDto.getFechaFin()!=""?buscarExpedienteDto.getFechaFin():"");
					param.setArea(buscarExpedienteDto.getArea());
					param.setIdArea(buscarExpedienteDto.getIdArea());
					System.out.println("param: "+param);
					List<ListExpedienteDto> listResponse =  expedienteService.listExpediente(param);
					return new ResponseEntity<>(listResponse, HttpStatus.OK);
				}catch (Exception e) {
					e.printStackTrace();
					return new ResponseEntity<>(e.getMessage(),HttpStatus.BAD_REQUEST);
				}
			}
	 
	 @PostMapping("/derivarExpediente")
		public ResponseEntity<?> derivarExpediente(@RequestBody DerivarExpedienteDto derivarExpedienteDto){
			System.out.println("controller");
			try {
				HashMap<Object, Object> response = new HashMap<>();
	   			response.put("descripcion", derivarExpedienteDto.getDescripcion());
	   			response.put("idusuario", derivarExpedienteDto.getIdusuario());
	   			response.put("idExpediente", derivarExpedienteDto.getIdExpediente());
	   			response.put("idArea", derivarExpedienteDto.getIdArea());
	            response.put("pdfExp", derivarExpedienteDto.getPdfExp());
	            response.put("result", "");
	            expedienteService.derivarExpediente(response);
				System.out.println("[insertProyecto respuesta]  "+response.get("respuesta"));
				Map<String, Object> res= new HashMap();
				res.put("result", response.get("respuesta"));
				return new ResponseEntity<>(res, HttpStatus.OK);
			} catch (Exception e) {
				e.printStackTrace();
				return new ResponseEntity<>(e.getMessage(),HttpStatus.BAD_REQUEST);
			}
		}
	
	 
	 @GetMapping("/listVerDetalle")
		public ResponseEntity<?> listverDetalleExpedienteDto(@RequestParam("idExpediente") Integer idExpediente){
			System.out.println("controller");
			try {
				HashMap<Object, Object> response = new HashMap<>();
	   			response.put("idExpediente", idExpediente);
				System.out.println("[insertProyecto respuesta]  "+response.get("respuesta"));
				List<ListVerDetalleExpedienteDto> listResponse =  expedienteService.listverDetalleExpedienteDto(idExpediente);
				return new ResponseEntity<>(listResponse, HttpStatus.OK);
			} catch (Exception e) {
				e.printStackTrace();
				return new ResponseEntity<>(e.getMessage(),HttpStatus.BAD_REQUEST);
			}
		}
	 
	 @GetMapping("/listSeguimientoExpExt")
		public ResponseEntity<?> listSeguiExpExt(@RequestParam("correlativo") String correlativo){
			System.out.println("controller");
			try {
				HashMap<Object, Object> response = new HashMap<>();
	   			response.put("correlativo", correlativo);
				System.out.println("[insertProyecto respuesta]  "+response.get("respuesta"));
				List<ListSeguimientoExternoDto> listResponse =  expedienteService.listSeguiExpExt(response);
				return new ResponseEntity<>(listResponse, HttpStatus.OK);
			} catch (Exception e) {
				e.printStackTrace();
				return new ResponseEntity<>(e.getMessage(),HttpStatus.BAD_REQUEST);
			}
		}
}

