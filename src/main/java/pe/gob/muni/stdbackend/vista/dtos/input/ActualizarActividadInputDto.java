package pe.gob.muni.stdbackend.vista.dtos.input;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ActualizarActividadInputDto {
	@NotNull(message = "El campo id de actividad no puede ser nulo")
	private Integer idActividad;

	private String codigoActividad;
	
	@NotNull(message = "El orden no puede ser nulo")
	private Integer orden;

	@NotBlank(message = "La actividad no puede ser nulo ni estar en blanco")
	private String actividad;
	private Integer activo;
}
