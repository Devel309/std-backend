package pe.gob.muni.stdbackend.vista.dtos.input.seguridad;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ActualizarNuevaClaveInputDto {
	@NotBlank(message = "La clave es obligatorio")
	@NotNull(message = "La clave es obligatorio")
	private String clave;
}