package pe.gob.muni.stdbackend.vista.dtos.output.seguridad;



import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ListaUsuariosAplicacionDatosOutputDto {
	
	private Integer resultado;
	private String mensaje;
	private List<ListaUsuarioOutputDto> lista;

}
