package pe.gob.muni.stdbackend.vista.controllers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import pe.gob.muni.stdbackend.negocio.modelos.BuscarEmpleadoDto;
import pe.gob.muni.stdbackend.negocio.modelos.EmpleadoDto;
import pe.gob.muni.stdbackend.negocio.modelos.ListEmpleadoDto;
import pe.gob.muni.stdbackend.negocio.servicios.interfaces.EmpleadoService;



@RestController
@RequestMapping("/apiEmpleado")
@CrossOrigin(origins = "*")
public class EmpleadoController {

	  @Autowired
	    EmpleadoService empleadoService;
	  
	@PostMapping("/insertEmpleado")
	public ResponseEntity<?> insertEmpleado(@RequestBody EmpleadoDto empleadoDto){
		System.out.println("controller");
		try {
			HashMap<Object, Object> response = new HashMap<>();
   			response.put("idPersona", empleadoDto.getIdPersona());
   			response.put("idArea", empleadoDto.getIdArea());
   			response.put("idCargo", empleadoDto.getIdCargo());
   			response.put("correlativo", empleadoDto.getCorrelativo());
            response.put("tdr", empleadoDto.getTdr());
            response.put("fechaInicio", empleadoDto.getFechaInicio());
            response.put("fechafin", empleadoDto.getFechafin());
            response.put("login", empleadoDto.getLogin());
            response.put("celular", empleadoDto.getCelular());
            response.put("correo", empleadoDto.getCorreo());
            response.put("descripcion", empleadoDto.getDescripcion());
            response.put("result", "");
            empleadoService.createRiesgo(response);
			System.out.println("[insertProyecto respuesta]"+response.get("respuesta"));
			Map<String, Object> res= new HashMap();
			res.put("result", response.get("respuesta"));
			return new ResponseEntity<>(res, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(),HttpStatus.BAD_REQUEST);
		}
	}
	
	  @PostMapping("/listEmpleado")
		public ResponseEntity<?> listEmpleado(@RequestBody BuscarEmpleadoDto buscarEmpleadoDto)
			{
				try {
					System.out.println(buscarEmpleadoDto);
					BuscarEmpleadoDto  param = new BuscarEmpleadoDto();
					param.setApNombres(buscarEmpleadoDto.getApNombres()!=""?buscarEmpleadoDto.getApNombres():"");
					param.setDni(buscarEmpleadoDto.getDni()>0?buscarEmpleadoDto.getDni():null);
					System.out.println("dni: "+param);
					List<ListEmpleadoDto> listResponse =  empleadoService.listEmpleado(param);
					return new ResponseEntity<>(listResponse, HttpStatus.OK);
				}catch (Exception e) {
					e.printStackTrace();
					return new ResponseEntity<>(e.getMessage(),HttpStatus.BAD_REQUEST);
				}
			}
		@PostMapping("/actualizarEmpleado")
		public ResponseEntity<?> updateEmpleado(@RequestBody EmpleadoDto empleadoDto){
			System.out.println("controller");
			try {
				HashMap<Object, Object> response = new HashMap<>();
				response.put("idEmpleado", empleadoDto.getIdEmpleado());
	   			response.put("idPersona", empleadoDto.getIdPersona());
	   			response.put("idArea", empleadoDto.getIdArea());
	   			response.put("idCargo", empleadoDto.getIdCargo());
	   			response.put("correlativo", empleadoDto.getCorrelativo());
	            response.put("tdr", empleadoDto.getTdr());
	            response.put("fechaInicio", empleadoDto.getFechaInicio());
	            response.put("fechafin", empleadoDto.getFechafin());
	            response.put("login", empleadoDto.getLogin());
	            response.put("celular", empleadoDto.getCelular());
	            response.put("correo", empleadoDto.getCorreo());
	            response.put("descripcion", empleadoDto.getDescripcion());
	            response.put("result", "");
	            empleadoService.updateEmpleado(response);
				System.out.println("[insertProyecto respuesta]"+response.get("respuesta"));
				Map<String, Object> res= new HashMap();
				res.put("result", response.get("respuesta"));
				return new ResponseEntity<>(res, HttpStatus.OK);
			} catch (Exception e) {
				e.printStackTrace();
				return new ResponseEntity<>(e.getMessage(),HttpStatus.BAD_REQUEST);
			}
		}
	  
}
