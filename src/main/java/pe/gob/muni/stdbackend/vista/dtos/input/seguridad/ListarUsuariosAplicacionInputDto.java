package pe.gob.muni.stdbackend.vista.dtos.input.seguridad;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;
@Data
public class ListarUsuariosAplicacionInputDto {
	
	@NotBlank(message = "El código de aplicación es obligatorio")
	@NotNull(message = "El código de aplicación es obligatorio")
	private String codigoAplicacion;
	
}
