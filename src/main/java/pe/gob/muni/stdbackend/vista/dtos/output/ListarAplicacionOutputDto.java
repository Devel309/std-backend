package pe.gob.muni.stdbackend.vista.dtos.output;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListarAplicacionOutputDto {
	private Integer idAplicacion;
	private String nombre;
	private String descripcion;
	private String codigo;
	private String estado;
	private String url;
	private Integer idUnidadOrganica;
	private String nombreUnidadOrganica;
}
