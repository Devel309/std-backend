package pe.gob.muni.stdbackend.vista.dtos.input.seguridad;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class LoginInputDto {
	@NotBlank(message = "El usuario no puede ser nulo ni estar en blanco")
	@NotNull(message = "El usuario no puede ser nulo")
	private String usuario;
	@NotBlank(message = "La Contraseña no puede ser nulo ni estar en blanco")
	@NotNull(message = "La Contraseña no puede ser nulo")
	private String clave;
	
	@NotBlank(message = "El captcha no puede ser nulo ni estar en blanco")
	@NotNull(message = "El captcha no puede ser nulo")
	private String recaptcha;	
	/*
	@NotNull(message = "El id de la aplicación no puede ser nulo")
	private Integer idAplicacion;
	*/
	@NotBlank(message = "El código no puede ser nulo ni estar en blanco")
	@NotNull(message = "El código no puede ser nulo")
	private String codigo;	

}
