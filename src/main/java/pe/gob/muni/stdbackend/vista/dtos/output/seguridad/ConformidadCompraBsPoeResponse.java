package pe.gob.muni.stdbackend.vista.dtos.output.seguridad;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class ConformidadCompraBsPoeResponse {

    private Integer idRegistroPedidoPoe;
    private String numeroRegistroPedidoPoe;
    private String numeroExpediente;
    private String razonSocialProveedor;
    private String concepto;
    private Integer idConformidadCompraBsPoePk;
    private Integer idOrdenCompraBsPoe;
    private String numeroContrato;
    private Date fechaIngreso;
    private Integer conformidad;
    private String observaciones;
    private Integer penalidad;
    private String numeroOrden;
}
