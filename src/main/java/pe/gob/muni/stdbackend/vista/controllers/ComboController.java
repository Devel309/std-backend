package pe.gob.muni.stdbackend.vista.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import pe.gob.muni.stdbackend.negocio.modelos.LisEmpleadoPersonaDto;
import pe.gob.muni.stdbackend.negocio.modelos.ListComboAreaDto;
import pe.gob.muni.stdbackend.negocio.modelos.ListComboCargoDto;
import pe.gob.muni.stdbackend.negocio.modelos.ListMotivoInstitucionDto;
import pe.gob.muni.stdbackend.negocio.servicios.interfaces.ListComboService;


@RestController
@RequestMapping("/apiCombo")
@CrossOrigin(origins = "*")
public class ComboController {

	@Autowired
	ListComboService listComboService;
	
	@GetMapping("/listComboArea")
	public ResponseEntity<?> listEmpleado() {
		try {
			List<ListComboAreaDto> listResponse = listComboService.listComboArea();
			return new ResponseEntity<>(listResponse, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	@GetMapping("/listComboCargo")
	public ResponseEntity<?> listComboCargo() {
		try {
			List<ListComboCargoDto> listResponse = listComboService.listComboCargo();
			return new ResponseEntity<>(listResponse, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	@GetMapping("/listComboMotivo")
	public ResponseEntity<?> listComboMotivo() {
		try {
			List<ListMotivoInstitucionDto> listResponse = listComboService.listComboMotivo();
			return new ResponseEntity<>(listResponse, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	@GetMapping("/listComboTema")
	public ResponseEntity<?> listComboTema() {
		try {
			List<ListMotivoInstitucionDto> listResponse = listComboService.listComboTema();
			return new ResponseEntity<>(listResponse, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("/listInstitucion")
	public ResponseEntity<?> listInstitucion(@RequestParam("ruc") String ruc) {
		try {
			System.out.println("dni" + ruc);
			ListMotivoInstitucionDto param = new ListMotivoInstitucionDto();
			param.setDesRuc(ruc != "" ? ruc : "");
			System.out.println("dni: " + param);
			List<ListMotivoInstitucionDto> listResponse = listComboService.listInstitucion(param);
			return new ResponseEntity<>(listResponse, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	@GetMapping("/listEmpleadoPersona")
	public ResponseEntity<?> lisEmpleadoPersona(@RequestParam("dni") Integer ruc) {
		try {
			System.out.println("dni" + ruc);
			ListMotivoInstitucionDto param = new ListMotivoInstitucionDto();
			param.setId(ruc != 0 ? ruc : null);
			System.out.println("dni: " + param);
			List<LisEmpleadoPersonaDto> listResponse = listComboService.lisEmpleadoPersona(param);
			return new ResponseEntity<>(listResponse, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
}
