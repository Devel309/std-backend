package pe.gob.muni.stdbackend.vista.dtos.input;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EliminarActividadInputDto {
	@NotNull(message = "El campo id de actividad no puede ser nulo")
	private Integer idActividad;
	private String usuarioModificacion;
}
