package pe.gob.muni.stdbackend.vista.dtos.output.seguridad;

import lombok.Getter;
import lombok.Setter;
import pe.gob.muni.stdbackend.vista.dtos.output.BaseOutputDto;

@Getter
@Setter
public class RefrescarTokenOutputDto extends BaseOutputDto {
	
	private String token;

}
