package pe.gob.muni.stdbackend.vista.dtos.output;

import lombok.*;

@Getter
@Setter
public class BaseOutputDto {
	private String mensaje;
	private Integer resultado;
}
