package pe.gob.muni.stdbackend.vista.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import pe.gob.muni.stdbackend.negocio.modelos.CapacitacionDto;
import pe.gob.muni.stdbackend.negocio.modelos.ListCapacitacionDto;
import pe.gob.muni.stdbackend.negocio.modelos.ListPapeletaDto;
import pe.gob.muni.stdbackend.negocio.modelos.PapeletaDto;
import pe.gob.muni.stdbackend.negocio.servicios.interfaces.SalidaPyCService;



@RestController
@RequestMapping("/apiSalida")
@CrossOrigin(origins = "*")
public class SalidaPyCController {

	@Autowired
	SalidaPyCService salidaPyCService;

	@PostMapping("/insertPapeleta")
	public ResponseEntity<?> createPapeleta(@RequestBody PapeletaDto papeletaDto) {
		System.out.println("controller");
		try {
			HashMap<Object, Object> response = new HashMap<>();
			response.put("lugarSalida", papeletaDto.getLugarSalida());
			response.put("lugarDestino", papeletaDto.getLugarDestino());
			response.put("fundamentacion", papeletaDto.getFundamentacion());
			response.put("descripcionPDF", papeletaDto.getDescripcionPDF());
			response.put("pdSustento", papeletaDto.getPdSustento());
			response.put("fSalida", papeletaDto.getFSalida());
			response.put("fRetorno", papeletaDto.getFRetorno());
			response.put("idMotivo", papeletaDto.getIdMotivo());
			response.put("idInstitucion", papeletaDto.getIdInstitucion());
			response.put("idEmpleado", papeletaDto.getIdEmpleado());
			response.put("result", "");
			salidaPyCService.createPapeleta(response);
			System.out.println("[insertProyecto respuesta]  " + response.get("respuesta"));
			Map<String, Object> res = new HashMap();
			res.put("result", response.get("respuesta"));
			return new ResponseEntity<>(res, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/listPapeleta")
	public ResponseEntity<?> listPapeleta() {
		try {
			ListPapeletaDto param = new ListPapeletaDto();
			System.out.println("dni: " + param);
			List<ListPapeletaDto> listResponse = salidaPyCService.listPapeleta();
			return new ResponseEntity<>(listResponse, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("/insertCapacitacion")
	public ResponseEntity<?> createCapacitacion(@RequestBody CapacitacionDto capacitacionDto) {
		System.out.println("controller");
		try {
			HashMap<Object, Object> response = new HashMap<>();
			response.put("fundamentacion", capacitacionDto.getFundamentacion());
			response.put("lugarSalida", capacitacionDto.getLugarSalida());
			response.put("lugarDestino", capacitacionDto.getLugarDestino());
			response.put("descripPDF", capacitacionDto.getDescripPDF());
			response.put("pdfSustento", capacitacionDto.getPdfSustento());
			response.put("fSalida", capacitacionDto.getFRetorno());
			response.put("fRetorno", capacitacionDto.getFRetorno());
			response.put("idTema", capacitacionDto.getIdTema());
			response.put("idInstitucion", capacitacionDto.getIdInstitucion());
			response.put("result", "");
			salidaPyCService.createCapacitacion(response);
			System.out.println("[insertProyecto respuesta]  " + response.get("respuesta"));
			Map<String, Object> res = new HashMap();
			res.put("result", response.get("respuesta"));
			return new ResponseEntity<>(res, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/listCapacitacion")
	public ResponseEntity<?> listCapacitacion() {
		try {
			ListPapeletaDto param = new ListPapeletaDto();
			System.out.println("dni: " + param);
			List<ListCapacitacionDto> listResponse = salidaPyCService.listCapacitacion();
			return new ResponseEntity<>(listResponse, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
}
