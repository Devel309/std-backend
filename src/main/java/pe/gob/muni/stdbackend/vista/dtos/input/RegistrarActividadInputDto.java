package pe.gob.muni.stdbackend.vista.dtos.input;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RegistrarActividadInputDto {

	private String codigoActividad;
	private Integer orden;
	private String actividad;
	private Integer activo;
}
