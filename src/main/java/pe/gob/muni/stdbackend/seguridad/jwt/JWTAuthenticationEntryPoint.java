package pe.gob.muni.stdbackend.seguridad.jwt;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import org.springframework.stereotype.Component;

@Component
public class JWTAuthenticationEntryPoint extends BasicAuthenticationEntryPoint {

	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException authException) throws IOException {

		final String jsonLoginResponse = getInvalidLoginJSONResponse();

		// https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/WWW-Authenticate
		response.addHeader("WWW-Authenticate", "Basic realm=\"" + getRealmName() + "\"");

		response.setContentType(MediaType.APPLICATION_JSON_VALUE);
		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		response.getWriter().println(jsonLoginResponse);
	}

	protected String getInvalidLoginJSONResponse() {
		/*
		 * final InvalidLoginResponse invalidLoginResponse = new InvalidLoginResponse();
		 * return new Gson().toJson(invalidLoginResponse);
		 */
		return "";
	}

	@Override
	public void afterPropertiesSet() {
		setRealmName("localhost");
		super.afterPropertiesSet();
	}
}
