package pe.gob.muni.stdbackend.seguridad.jwt;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.DefaultClaims;
import pe.gob.muni.stdbackend.transversal.constantes.ConstanteJwt;
import pe.gob.muni.stdbackend.transversal.utilidades.Funciones;

@Component
public class JWTTokenProvider {

	public String generarTokenUsuarioPermanente(JwtUsuarioBean usuario) {
		final LocalDateTime now = LocalDateTime.now();

		final Map<String, Object> claims = new HashMap<>();
		claims.put("idUsuario", usuario.getIdUsuario());
		claims.put("idAplicacion", usuario.getIdAplicacion());
		claims.put("numeroDocumento", usuario.getNumeroDocumento());
		claims.put("nombres", usuario.getNombres());
		claims.put("apellidoPaterno", usuario.getApellidoPaterno());
		claims.put("apellidoMaterno", usuario.getApellidoMaterno());
		claims.put("tipoSesion", usuario.getTipoSesion());
		claims.put("idSesion", usuario.getIdSesion());

		long nowMillis = System.currentTimeMillis();
		Date horaActual = new Date(nowMillis);

		JwtBuilder builder = Jwts.builder().setSubject(usuario.getNumeroDocumento())
				// .setIssuedAt(horaActual)
				.setClaims(claims).setIssuedAt(horaActual).signWith(SignatureAlgorithm.HS512, ConstanteJwt.SECRET);

		if (ConstanteJwt.EXPIRATION_TIME_MILISECONDS > 0) {
			long expMillis = nowMillis + ConstanteJwt.EXPIRATION_TIME_MILISECONDS;
			Date exp = new Date(expMillis);
			System.out.println("Fecha expira en: " + exp);
			builder.setExpiration(exp);
		}
		//
		return builder.compact();
	}
	
	public String generarTokenUsuarioSinPersona(JwtUsuarioBean usuario) {
		final LocalDateTime now = LocalDateTime.now();

		final Map<String, Object> claims = new HashMap<>();
		claims.put("idUsuario", usuario.getIdUsuario());
		claims.put("idAplicacion", usuario.getIdAplicacion());
		claims.put("nombreUsuario", usuario.getNombreUsuario());
		claims.put("tipoSesion", usuario.getTipoSesion());
		claims.put("idSesion", usuario.getIdSesion());
		
		long nowMillis = System.currentTimeMillis();
		Date horaActual = new Date(nowMillis);

		JwtBuilder builder = Jwts.builder().setSubject(usuario.getNombreUsuario())
				// .setIssuedAt(horaActual)
				.setClaims(claims).setIssuedAt(horaActual).signWith(SignatureAlgorithm.HS512, ConstanteJwt.SECRET);

		if (ConstanteJwt.EXPIRATION_TIME_MILISECONDS > 0) {
			long expMillis = nowMillis + ConstanteJwt.EXPIRATION_TIME_MILISECONDS;
			Date exp = new Date(expMillis);
			System.out.println("Fecha expira en: " + exp);
			builder.setExpiration(exp);
		}
		//
		return builder.compact();
	}
	
	

	public String generarRefreshToken(JwtUsuarioBean usuario) {
		final LocalDateTime now = LocalDateTime.now();
		final LocalDateTime expiryDate = now.plus(ConstanteJwt.EXPIRATION_TIME_SECONDS, ChronoUnit.MILLIS);

		final Map<String, Object> claims = new HashMap<>();
		claims.put("idUsuario", usuario.getIdUsuario());
		claims.put("idAplicacion", usuario.getIdAplicacion());
		claims.put("numeroDocumento", usuario.getNumeroDocumento());
		claims.put("nombres", usuario.getNombres());
		claims.put("apellidoPaterno", usuario.getApellidoPaterno());
		claims.put("apellidoMaterno", usuario.getApellidoMaterno());

		long nowMillis = System.currentTimeMillis();
		Date horaActual = new Date(nowMillis);

		long expMillis = nowMillis + ConstanteJwt.EXPIRATION_TIME_MILISECONDS;
		Date exp = new Date(expMillis);

		return Jwts.builder().setClaims(claims).setSubject(usuario.getNumeroDocumento())
				.setIssuedAt(new Date(System.currentTimeMillis())).setExpiration(exp)
				.signWith(SignatureAlgorithm.HS512, ConstanteJwt.SECRET).compact();

	}

	public String generarRefreshToken(final Map<String, Object> claims, String ruc) {
		final LocalDateTime now = LocalDateTime.now();
		final LocalDateTime expiryDate = now.plus(ConstanteJwt.EXPIRATION_TIME_SECONDS, ChronoUnit.MILLIS);
		long nowMillis = System.currentTimeMillis();
		Date horaActual = new Date(nowMillis);

		long expMillis = nowMillis + ConstanteJwt.EXPIRATION_TIME_MILISECONDS;
		Date exp = new Date(expMillis);

		return Jwts.builder().setClaims(claims).setSubject(ruc).setIssuedAt(horaActual).setExpiration(exp)
				.signWith(SignatureAlgorithm.HS512, ConstanteJwt.SECRET).compact();

	}

	// Validate the token
	public Claims validateAuthToken(final String token) {
		// just parsing correctly means its valid
		return getClaimsFromAuthToken(token);
	}

	// Get user Id from token
	public String getUserIdFromAuthToken(Claims claims) {
		String ruc = (String) claims.get("ruc");
		return ruc;
	}

	// JWT special Claims
	Claims getClaimsFromAuthToken(String token) {
		return Jwts.parser().setSigningKey(ConstanteJwt.SECRET).parseClaimsJws(token).getBody();
	}

	public HashMap<String, Object> obtenerPayload(String token) {
		String tokenOnly = token.substring(ConstanteJwt.BEARER_TOKEN_PREFIX.length());
		DefaultClaims claims = (DefaultClaims) this.validateAuthToken(tokenOnly);

		Map<String, Object> expectedMap = Funciones.getMapFromIoJsonwebtokenClaims(claims);
		return (HashMap<String, Object>) expectedMap;
	}

	public String obtenerSoloToken(String token) {
		String tokenOnly = token.substring(ConstanteJwt.BEARER_TOKEN_PREFIX.length());
		return tokenOnly;
	}
	
	public Object obtenerValorToken(String token, String nombreCampo) {
			HashMap<String, Object> claimw = obtenerPayload(token);		
			Object valor = claimw.get(nombreCampo);
		return valor;
	}

}

