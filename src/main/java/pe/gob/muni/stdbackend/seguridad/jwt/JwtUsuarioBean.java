package pe.gob.muni.stdbackend.seguridad.jwt;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class JwtUsuarioBean {
	private Integer idUsuario;
	private String correo;
	private String numeroDocumento;
	private Integer idAplicacion;
	private String codigoAplicacion;
	private String nombres;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String nombreUsuario;
	private String idSesion;
	private Integer tipoSesion;
	
}
