package pe.gob.muni.stdbackend.seguridad.filtros;

import java.io.IOException;
import java.util.Collections;

import javax.servlet.FilterChain;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import com.google.gson.Gson;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.UnsupportedJwtException;
import pe.gob.muni.stdbackend.seguridad.jwt.JWTTokenProvider;
import pe.gob.muni.stdbackend.transversal.constantes.ConstanteApi;
import pe.gob.muni.stdbackend.transversal.constantes.ConstanteJwt;
import pe.gob.muni.stdbackend.transversal.excepciones.ExceptionResponse;
import pe.gob.muni.stdbackend.transversal.properties.CrossOriginProperties;
import pe.gob.muni.stdbackend.transversal.properties.SeguridadValidarProperties;
import pe.gob.muni.stdbackend.transversal.properties.ServerServleProperties;
 

public class JWTAuthenticationFilter extends OncePerRequestFilter {

	@Autowired
	private JWTTokenProvider tokenProvider;

	@Autowired
	private ServerServleProperties serverServleProperties;

	@Autowired
	private SeguridadValidarProperties seguridadValidarProperties;

	@Autowired
	private CrossOriginProperties crossOriginProperties;

	private Gson gson = new Gson();

	public String getCrossOrigin(HttpServletRequest request) {
		if( !crossOriginProperties.getOriginsHabilitado()) {
			return "*";
		}
		String origin = request.getHeader("origin");
		logger.info("SIGEA - DOMAIN ORIGIN:"+origin);
		for(String tmp : crossOriginProperties.getAllowedOrigins()){
			if(origin != null && tmp.trim().contains(origin)){
				return origin;
			}
		}
		
		logger.info("SIGEA - origin"+crossOriginProperties.getAllowedOrigins().get(0));
		
		// cuando "origin" es null, indica que es del mismo dominio, por eso
		// se debe retornar el primer dominio que es el mismo en el properties.
		return crossOriginProperties.getAllowedOrigins().get(0);
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {

		logger.info("SIGEA - INI: " + request.getRequestURI());
//		String origin = this.getUrl(request);
		// TODO Auto-generated method stub
		response.setHeader("X-FRAME-OPTIONS", "DENY");
		response.setHeader("Access-Control-Allow-Origin", this.getCrossOrigin(request));
		response.setHeader("Access-Control-Allow-Methods", "GET,POST,DELETE,PUT,OPTIONS");
		response.setHeader("Access-Control-Allow-Headers", "*");
		response.setHeader("Access-Control-Allow-Credentials", "true");
		response.setHeader("Access-Control-Max-Age", "180");
		// TODO Auto-generated method stub
		if ("OPTIONS".equalsIgnoreCase(((HttpServletRequest) request).getMethod())) {
			response.setStatus(HttpServletResponse.SC_OK);
			//System.out.println("INGRESE OPTIONS");
			return;
			// filterChain.doFilter(request, response);
		}

		try {
			final String authenticationHeader = request.getHeader(ConstanteJwt.HEADER_STRING);
			logger.info("SIGEA - Header: " + authenticationHeader);
			//System.out.println("INGRESE OTRO METODO");
			
			//System.out.println("INGRESE OTRO METODO " + ((HttpServletRequest) request).getHeader("Authorization"));
			
			// System.out.println("INGRESE CONTROLLER");
			// String url = getFullURL(request);
			String uri = request.getRequestURI();
			if (esUrlLibre(uri, request)) {
				filterChain.doFilter(request, response);
			} else {
				if (seguridadValidarProperties.getToken()) {
					System.out.println("INGRESE TOKEN");
					if (checkJWTToken(request)) {
						String jwt = getJWTFromRequest(request);
						Claims claims = tokenProvider.validateAuthToken(jwt);

						if (claims.get("numeroDocumento") != null|| claims.get("nombreUsuario") != null) {

							Integer idAplicacion = (Integer) claims.get("idAplicacion");
							Integer idUsuario = (Integer) claims.get("idUsuario");
							//Usuario usuario = usuarioServicio.obtenerInfoUsuarioPorUsuarioAplicacion(idUsuario,idAplicacion);
							Integer  sessionUnica = (Integer) claims.get("tipoSesion");
							filterChain.doFilter(request, response);
						} else {
							SecurityContextHolder.clearContext();
							validarError(response, "El token no cuenta con la clave principal.");

							// response.setStatus(HttpServletResponse.SC_FORBIDDEN);
							// response.sendError(HttpServletResponse.SC_FORBIDDEN,
							// "El token no cuenta con la clave principal.");
						}
					} else {
						SecurityContextHolder.clearContext();
						validarError(response, "Se requiere token para acceder al servicio.");
						response.setStatus(HttpServletResponse.SC_FORBIDDEN);
						// response.sendError(HttpServletResponse.SC_FORBIDDEN,
						// "Se requiere token para acceder al servicio.");
					}
				} else {
					System.out.println("SIGEA - INGRESE NO TOKEN");
					logger.info("SIGEA - request"+request.getRequestURI());
					filterChain.doFilter(request, response);
				}
			}

		} catch (ExpiredJwtException e) {
			String isRefreshToken = request.getHeader("isRefreshToken");
			String requestURL = request.getRequestURL().toString();
			// allow for Refresh Token creation if following conditions are true.
			if (isRefreshToken != null && isRefreshToken.equals("true") && requestURL.contains("refreshtoken")) {
				allowForRefreshToken(e, request);
			} else {
				request.setAttribute("exception", e);
				logger.error("El token ha expirado.", e);
				response.setStatus(HttpServletResponse.SC_FORBIDDEN);
				response.sendError(HttpServletResponse.SC_FORBIDDEN, e.getMessage());
			}
		} catch (UnsupportedJwtException e) {
			logger.error("No se puede detectar la clave del token.", e);
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			response.sendError(HttpServletResponse.SC_FORBIDDEN, e.getMessage());
		} catch (MalformedJwtException e) {
			logger.error("El token esta mal formado.", e);
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			response.sendError(HttpServletResponse.SC_FORBIDDEN, e.getMessage());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			validarError(response, "Ocurrió un error interno.");
		}


	}

	private void validarError(HttpServletResponse response, String mensaje) throws IOException {
		ExceptionResponse error = new ExceptionResponse();
		error.setMensaje(mensaje);
		error.setMensajeInteno(mensaje);
		error.setResultado(-1);

		String errorJson = gson.toJson(error);
		response.setStatus(HttpServletResponse.SC_FORBIDDEN);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(errorJson);
	}

	private void allowForRefreshToken(ExpiredJwtException ex, HttpServletRequest request) {

		// create a UsernamePasswordAuthenticationToken with null values.
		UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
				null, null, null);
		// After setting the Authentication in the context, we specify
		// that the current user is authenticated. So it passes the
		// Spring Security Configurations successfully.
		SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
		// Set the claims so that in controller we will be using it to create
		// new JWT
		request.setAttribute("claims", ex.getClaims());

	}

	/**
	 * Authentication method in Spring boot
	 *
	 * @param claims
	 * @param request
	 */
	private void setUpSpringAuthentication(Claims claims, HttpServletRequest request) {

		// get user ID
		final String userId = tokenProvider.getUserIdFromAuthToken(claims);


	}

	private String getJWTFromRequest(HttpServletRequest request) {

		final String bearerToken = request.getHeader(ConstanteJwt.HEADER_STRING);

		if (StringUtils.hasText(bearerToken) && bearerToken.startsWith(ConstanteJwt.BEARER_TOKEN_PREFIX)) {
			return bearerToken.substring(ConstanteJwt.BEARER_TOKEN_PREFIX.length());
		}

		throw new MalformedJwtException("Authentication Header doesn't have Bearer Token");
	}

	private boolean checkJWTToken(HttpServletRequest request) {

		final String authenticationHeader = request.getHeader(ConstanteJwt.HEADER_STRING);
		//System.out.println("Header: " + authenticationHeader);
		return authenticationHeader != null && authenticationHeader.startsWith(ConstanteJwt.BEARER_TOKEN_PREFIX);

	}

	private static String getFullURL(HttpServletRequest request) {

		StringBuilder requestURL = new StringBuilder(request.getRequestURL().toString());
		String queryString = request.getQueryString();

		if (queryString == null) {
			return requestURL.toString();
		} else {
			return requestURL.append('?').append(queryString).toString();
		}

	}

	private Boolean esUrlLibre(String url, HttpServletRequest request) {

		String urlTmp = "";
		for (String urlLibre : ConstanteApi.URL_WEB_LIBRES) {
//			urlTmp = serverServleProperties.getContextPath() + urlLibre;
			urlTmp = request.getContextPath() + urlLibre;
			if (url.contains(urlTmp)) {
				return true;
			}
		}
		for (String urlLibre : ConstanteApi.APIS_LIBRES) {
//			urlTmp = serverServleProperties.getContextPath() + urlLibre;
			urlTmp = request.getContextPath() + urlLibre;
			if (url.contains(urlTmp)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Returns a name for this context
	 * 
	 * @param servletContext the servlet context
	 * @return name for this context
	 */
	private static String getContextName(ServletContext servletContext) {
		String name = "lutece";

		if (servletContext != null) {
			String contextName = servletContext.getServletContextName();

			if (contextName == null) {
				contextName = servletContext.getContextPath();
			}

			if (org.apache.commons.lang3.StringUtils.isNotBlank(contextName)) {
				name = contextName;
			}

		}

		return name;
	}
}

