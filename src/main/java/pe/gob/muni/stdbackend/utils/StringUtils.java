package pe.gob.muni.stdbackend.utils;

import java.text.Normalizer;

public class StringUtils {

	public static String toAscii(String arg) {
		if(arg!=null) {
			StringBuilder sb = new StringBuilder(arg.length());
	        arg = Normalizer.normalize(arg, Normalizer.Form.NFD);
	        for (char c : arg.toCharArray()) {
	            if (c <= '\u007F') sb.append(c);
	        }
	        return sb.toString();
		} // end-if
        return arg;
    }
	
	public static String quitarEspacios(String arg) {
		if(arg!=null) {
			return arg.replaceAll("\\s","");
		} // end-if
		return arg;
	}
	
	public static String convertirMayuscula(String arg) {
		if(arg!=null) {
			return arg.toUpperCase();
		} // end-if
		return arg;
	}
	
}
