package pe.gob.muni.stdbackend.utils;

public class VariablesReporte {

	public static final String IMG = "imagenes/logo/logo_onpe.png";
	public static final String NAME_TEMP_GENERAL = "reportes";
	public static final String NAME_TEMP_REPORTE = "/reportes/";
	
	public static final String PEDIDO_COMPRA = "/PEDIDO_COMPRA.jasper";
	public static final String PEDIDO_SERVICIO = "/PEDIDO_SERVICIO.jasper";
	public static final String AVANCE_PEDIDO_COMPRA = "/AVANCE_PEDIDO_COMPRA.jasper";
	public static final String AVANCE_PEDIDO_SERVICIO = "/AVANCE_PEDIDO_SERVICIO.jasper";
	public static final String ORDEN_COMPRA = "/ORDEN_COMPRA.jasper";
	public static final String ORDEN_SERVICIO = "/ORDEN_SERVICIO.jasper";
	public static final String TERMINO_REFERENCIA = "/TERMINO_REFERENCIA.jasper";
	public static final String VERIFICACION_SERVICIO = "/VERIFICACION_SERVICIO.jasper";
	public static final String VERIFICACION_COMPRA = "/VERIFICACION_COMPRA.jasper";
	public static final String CUADRO_COMPARATIVO_SERVICIO = "/CUADRO_COMPARATIVO_SERVICIO.jasper";
	public static final String CONFORMIDAD_SERVICIO = "/CONFORMIDAD_SERVICIO.jasper";
	public static final String CONFORMIDAD_PARCIAL_SERVICIO = "/CONFORMIDAD_PARCIAL_SERVICIO.jasper";
	public static final String PEDIDO_ESPECIFICACION_TECNICA = "/REPORTE_ESPECIFICACION_TECNICA.jasper";
	public static final String CUADRO_COMPARATIVO_BIEN = "/CUADRO_COMPARATIVO_BIEN.jasper";
	public static final String CONFORMIDAD_COMPRA = "/CONFORMIDAD_COMPRA.jasper";
	public static final String REEMBOLSO_FONDO_CAJA_CHICA = "/REEMBOLSO_FONDO_CAJA_CHICA.jasper";
	public static final String SOLICITUD_CERTIFICACION_PEDIDO = "/SOLICITUD_CERTIFICACION_PEDIDO.jasper";
	
	public static final String PEDIDO_VIATICO = "/PEDIDO_VIATICO.jasper";
	public static final String CREDITO_ANULACION = "/CREDITO_ANULACION.jasper";
}
