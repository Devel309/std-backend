package pe.gob.muni.stdbackend.utils;

import java.io.IOException;
import java.util.Base64;

public class Constant {
	public static final String MSJ_ERROR="Hemos encontrado un problema al realizar la operación.";
	public static final String MSJ_EXITO="Operación realizada con éxito";
	public static final String MSJ_EXISTE_NOMBRE="Nombre de horario ya registrado";
	public static final String MSJ_HORARIO_EN_USO="No puede eliminar el horario, está en uso";
	public static final String MSJ_DESCANSO_COMPENSACION="DESCANSO POR COMPENSACIÓN";
	
	public static final Integer ESTADO_ACTIVO = 1;
	public static final Integer ESTADO_DESHABILITADO = 0;
	
	public static final Integer INCIDENTE_ESTADO_CREADO=0;
	public static final Integer INCIDENTE_ESTADO_ENVIADO=1;
	public static final Integer INCIDENTE_ESTADO_APROBADO=2;
	public static final Integer INCIDENTE_ESTADO_VALIDADO=3;
	public static final Integer INCIDENTE_ESTADO_RECHAZO =4;
	
	public static final Integer SOLICITUD_ESTADO_CREADO=0;
	public static final Integer SOLICITUD_ESTADO_ENVIADO=1;
	public static final Integer SOLICITUD_ESTADO_APROBADO=2;
	public static final Integer SOLICITUD_ESTADO_VALIDADO=3;
	public static final Integer SOLICITUD_ESTADO_RECHAZO =4;
	
	//Estados para vacaciones
	public static final Integer PERIODO_VACACION_PROGRAMADA=1;
	public static final Integer PERIODO_VACACION_CERRADA =2;
	public static final Integer PERIODO_VACACION_REPROGRAMADA=3;
	
	public static final String TIPO_MENSAJE_ALERTA_ROJO = "3";
	

	public static final String ANIOS_FILTROS="ANIOS_FILTROS";
	public static final String DIA_CORTE="DIA_CORTE";

	public static final Integer SOLICITUD_VACACIONES_CREADA = 0;
	public static final Integer SOLICITUD_VACACIONES_ENVIADA = 1;
	
	public static final String DIAS_HABILES_VACACIONES = "DIAS_HABILES_VACACIONES";
	public static final String HORA_SALIDA="17:00:00";
	
	public static final int TIPO_INCIDENTE_FALTA_POR_TARDANZA = 12;
	public static final int TIPO_INCIDENTE_TARDANZA = 13;
	public static final int TIPO_INCIDENTE_FALTA = 14;
	public static final int TIPO_INCIDENTE_UNA_SOLA_MARCACION = 15;
	public static final int TIPO_INCIDENTE_SALIDA_ANTICIPADA = 16;
	public static final int TIPO_INCIDENTE_MARCACIONES_MULTIPLES = 17;
	public static final int TIPO_INCIDENTE_FALTA_POR_SALIDA_ANTICIPADA = 20;
	public static final int TIPO_INCIDENTE_LIBRE = 21;
	public static final int TIPO_INCIDENTE_INCIDENTE_GENERADO = 22;
	
	
	public static final int TIPO_SOLICITUD_MOTIVO_AM = 1;
	public static final int TIPO_SOLICITUD_MOTIVO_CS = 2;
	public static final int TIPO_SOLICITUD_MOTIVO_ASUNT_PART = 3;
	public static final int TIPO_SOLICITUD_MOTIVO_CAP = 4;
	public static final int TIPO_SOLICITUD_MOTIVO_ONO = 5;
	public static final int TIPO_SOLICITUD_MOTIVO_AUT_ING= 6;
	public static final int TIPO_SOLICITUD_MOTIVO_DM= 7;
	public static final int TIPO_SOLICITUD_MOTIVO_OMI = 8;
	public static final Integer TIPO_SOLICITUD_MOTIVO_PER_SIN = 9;
	public static final Integer TIPO_SOLICITUD_MOTIVO_PER_LIC_ENF = 10;
	public static final Integer TIPO_SOLICITUD_MOTIVO_PATER = 11;
	public static final Integer TIPO_SOLICITUD_MOTIVO_PD= 12;
	public static final Integer TIPO_SOLICITUD_MOTIVO_DUELO = 13;
	public static final Integer TIPO_SOLICITUD_MOTIVO_LAC = 14;
	public static final Integer TIPO_SOLICITUD_MOTIVO_PPN = 15;
	public static final Integer TIPO_SOLICITUD_MOTIVO_COMP = 16;
	public static final Integer TIPO_SOLICITUD_MOTIVO_CITACION = 17;
	public static final Integer TIPO_SOLICITUD_MOTIVO_DOCE = 18;

	public static final String FILTRO = "filtro";
	public static final String MONITOR = "monitor";
	public static final String PROCESO_FILTRO = "procesoFiltro";
	public static final String REDIRECT_LOGIN = "redirect:/login";
	public static final String REDIRECT_MAIN = "redirect:/main";
	public static final String USUARIO = "usuario";
	public static final String USUARIO_FILTRO = "usuarioFiltro";
	
	public static final int TIPO_REGIMEN_CAP = 1;
	public static final String TIPO_REGIMEN_CAP_D = "CAP";
	public static final int TIPO_REGIMEN_CAS = 2;
	public static final String TIPO_REGIMEN_CAS_D = "CAS";
	
	public static final int SGP_GOECOR = 2;
	
	//PERFILES
    public static final String PROFILE_SUPERADMIN = "Super Admin";
    public static final String PROFILE_JEFE_ODPE = "Jefe de ODPE";
    public static final String PROFILE_MONITOR_GOECOR = "Monitor PA";
    public static final String PROFILE_ADMINISTRADOR_GOECOR = "Administrador PA";
    public static final String PROFILE_ADMINISTRADOR_POE = "Administrador POE";
    public static final String PROFILE_MONITOR_ORGANO = "Monitor de Organo POE";
    public static final String PROFILE_MONITOR_GPP = "Monitor POE";
    public static final String PROFILE_MONITOR_ODPE = "Usuario de ODPE";
    public static final String PROFILE_JEFE_MONITOR_PA = "Jefe de Monitores PA";
    public static final String PROFILE_SUPERVISOR_PA = "Supervisor PA";
    public static final String PROFILE_SUPERVISOR_POE = "Supervisor POE";
    public static final String PROFILE_ANALISTA = "Analista PA";
    public static final String PROFILE_COORDINADOR = "Coordinador PA";
    public static final String PROFILE_SUPERVISOR_ODPE = "Supervisor ODPE";
    public static final String PROFILE_CENTRO_SOPORTE = "Centro Soporte";
    
	public static final Integer PEFIL_EMPLEADO = 3;
	
	public static final String CLAVE_DEFECTO = "$2a$10$C0NDF/JzTvbzBUtrmTk2l.VjT.sN1cCaS5xw/Rz8rw8u3ZLQhsZZu";
	
	public static final String CADENA_FOTO = "data:image/png;base64,";
	
	public static final String CARGO_GERENTE = "GERENTE";
	public static final String ANIO_MINIMO_FILTRO_REPORTE = "ANIO_MINIMO_FILTRO_REPORTE";
	public static final String SUBJECT_CORREO_DOCUMENTO = "NOTIFICACIONES - SISTEMA DE GESTIÓN DE RIESGOS SGR - ONPE";
	public static final String POR_VENCER_3_DIAS = "POR VENCER EN 3 DIAS.";
	public static final String POR_VENCER_2_DIAS = "POR VENCER EN 2 DIAS.";
	public static final String POR_VENCER_1_DIA = "POR VENCER EN 1 DIAS.";
	public static final String POR_VENCER_0_DIA = "POR VENCER EN 0 DIAS.";
	public static final String ACTIVIDAD_CONTROL_PROXIMO_VENCER = "a su nombre próxima a vencer.";
	public static final String ACTIVIDAD_CONTROL_VENCIDA = "vencida a su nombre.";
	
	public static String jwtSecret="onpe-clave-secret";
	//public static int jwtExpirationMs= 300000;
	public static int jwtExpirationMs= 3600000;
	//public static int jwtExpirationMs= 120000;

	public static int jwtRefreshExpirationMs= jwtExpirationMs*2;

	public static byte[] convertFile(String file_string) throws IOException {
		byte[] bytes = Base64.getDecoder().decode(file_string);
		return bytes;
	}

	public static String convertFileToString(byte[] archivo) throws IOException {
		String encodeArchivo = Base64.getEncoder().encodeToString(archivo);
		return encodeArchivo;
	}
	
	 enum Level {
		 	TIPO_INCIDENTE_FALTA_POR_TARDANZA,
		    MEDIUM,
		    HIGH
		  }

}
