package pe.gob.muni.stdbackend;

import java.awt.Color;
/*import java.io.FileOutputStream;
import java.math.BigDecimal;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;




public class main {
	private static String FILE = "C:/Users/ctang/Desktop/GenerarPDF/ejemploBase.pdf";
    private static Font catFont = new Font(Font.FontFamily.HELVETICA, 18,Font.BOLD);
    private static Font titulo= new Font(Font.FontFamily.HELVETICA, 13f, Font.BOLD);
    private static Font subTitulo= new Font(Font.FontFamily.HELVETICA, 11f, Font.BOLD);
    private static Font contenido= new Font(Font.FontFamily.HELVETICA, 10f, Font.NORMAL);
    private static Font subTituloTabla= new Font(Font.FontFamily.HELVETICA, 11f, Font.NORMAL);

    static Font normal= new Font(Font.FontFamily.HELVETICA, 11f);
            
	public static void main(String[] args) {

		try {
			Document document = new Document(PageSize.A4,40,40,40,40);
            PdfWriter.getInstance(document, new FileOutputStream(FILE));
            document.open();
            addMetaData(document);
            addTitlePage(document);
            //addContent(document);
            document.close();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
	}
	private static void addMetaData(Document document) {
        document.addTitle("Mi PDF");
    }
	private static void addTitlePage(Document document) throws DocumentException {
        Paragraph preface = new Paragraph();
        
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("ONPE PRUEBA N° 01", catFont));
        preface.setAlignment(Element.ALIGN_CENTER); 


    }
	
	//----- METODOS----
	private static void setearRowsColspanStatic(String dato, Integer row,Integer colum, PdfPTable table){

        PdfPCell c = new PdfPCell(new Phrase(dato,contenido));
        Color colorBody = new Color(242 , 242 , 242);
        //c.setBackgroundColor(colorBody);
        c.setPadding(5);
        c.setHorizontalAlignment(Element.ALIGN_CENTER);
        c.setVerticalAlignment(Element.ALIGN_CENTER);
        c.setColspan(colum);
        c.setRowspan(row);
        table.addCell(c);
    }
    private static void setearBodyStaticLetraPequenio(String dato, Integer colum, PdfPTable table){
        Font pequenio = new Font(Font.FontFamily.HELVETICA, 9,
            Font.BOLD);
        PdfPCell c = new PdfPCell(new Phrase(dato, pequenio));
        Color colorBody = new Color(242 , 242 , 242);
        c.setColspan(colum);
        //c.setBackgroundColor(colorBody);
        c.setPadding(5);
        table.addCell(c);
    }

    private static void setearCellcabezera(String dato, Integer colum, PdfPTable table){

        Font title = new Font(Font.FontFamily.HELVETICA, 14,Font.BOLD);
        PdfPCell c = new PdfPCell(new Phrase(dato,title));
        Color colorHeader = new Color(189 , 214 , 238);
        c.setHorizontalAlignment(Element.ALIGN_CENTER);
        c.setColspan(colum);
        //c.setBackgroundColor(colorHeader);
        c.setPadding(5);
        table.addCell(c);
        table.setHeaderRows(1);
    }
    private static void setearSubTitle(String dato, Integer colum, PdfPTable table){
        Font subTitle = new Font(Font.FontFamily.HELVETICA, 12,Font.BOLD);
        Color colorSubTitle = new Color(242 , 242 , 242);
        PdfPCell c = new PdfPCell(new Phrase(dato,subTitle));
        c.setColspan(colum);
        //c.setBackgroundColor(colorSubTitle);
        c.setPadding(5);
        table.addCell(c);
        table.setHeaderRows(1);
    }
    private static void setearBodyStatic(String dato, Integer colum, PdfPTable table){
        PdfPCell c = new PdfPCell(new Phrase(dato,contenido));
        Color colorBody = new Color(242 , 242 , 242);
        c.setColspan(colum);
        //c.setBackgroundColor(colorBody);
        c.setPadding(5);
        //c.setBorder(0);
        table.addCell(c);
    }   
    private static void setearBodyData(String dato, Integer colum, PdfPTable table){
        PdfPCell c = new PdfPCell(new Phrase(dato,contenido));
        c.setColspan(colum);
        c.setPadding(5);
        table.addCell(c);
    }

    private static void setearBodyDataSborder(String dato, Integer colum, PdfPTable table){
        PdfPCell c = new PdfPCell(new Phrase(dato,titulo));
        c.setColspan(colum);
        c.setPadding(5);
        c.setBorder(0);
        table.addCell(c);
    }

    private static void setearBodyDataBorderInferior(String dato, Integer colum, PdfPTable table){
        PdfPCell c = new PdfPCell(new Phrase(dato,contenido));
        c.setColspan(colum);
        c.setPadding(5);
        c.setBorder(0);
        c.setBorderWidthBottom(0.5f);
        table.addCell(c);
    }

    private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }
	
}*/
