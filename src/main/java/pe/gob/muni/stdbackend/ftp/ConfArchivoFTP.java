package pe.gob.muni.stdbackend.ftp;

public class ConfArchivoFTP {

	private Long maxCharacters;
	private String[] validatedExtensions;
	private Long maxSize;

	public Long getMaxCharacters() {
		return maxCharacters;
	}

	public void setMaxCharacters(Long maxCharacters) {
		this.maxCharacters = maxCharacters;
	}

	public String[] getValidatedExtensions() {
		return validatedExtensions;
	}

	public void setValidatedExtensions(String[] validatedExtensions) {
		this.validatedExtensions = validatedExtensions;
	}

	public Long getMaxSize() {
		return maxSize;
	}

	public void setMaxSize(Long maxSize) {
		this.maxSize = maxSize;
	}

	
}
