package pe.gob.muni.stdbackend.ftp;


import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import pe.gob.muni.stdbackend.transversal.constantes.Constantes;
import pe.gob.muni.stdbackend.transversal.excepciones.CustomBussinessException;
import pe.gob.muni.stdbackend.utils.FileUtils;   

 


public class ArchivoFTP {

	private final MultipartFile multipartFile;
	private final ConfArchivoFTP configuration;
	public static Map<String, String> MIME_TYPES = null; 
	
	static {
		MIME_TYPES = new HashMap<String, String>();
		MIME_TYPES.put("pdf", "application/pdf");
		MIME_TYPES.put("jpeg","image/jpeg");
		MIME_TYPES.put("jpg", "image/jpeg");
		MIME_TYPES.put("rar", "application/x-rar-compressed");
		MIME_TYPES.put("zip", "application/zip");
		MIME_TYPES.put("png", "image/x-png");
	}
	
	public ArchivoFTP(MultipartFile multipartFile, ConfArchivoFTP conf) {
		this.multipartFile = multipartFile;
		this.configuration = conf;
	}

	public MultipartFile getMultipartFile() {
		return multipartFile;
	}

	public String getMimeType() throws IOException {
		//String mimeType = URLConnection.guessContentTypeFromStream(multipartFile.getInputStream());
		String extension = this.getFileExtension();
		String mimeType = MIME_TYPES.get(extension.toLowerCase());
		return mimeType;
	}
	
	public String getFileName() {
		return StringUtils.cleanPath(multipartFile.getOriginalFilename());
	}
	
	public String getFileExtension() {
		return FileUtils.getFileExtension(this.getFileName());
	}
	
	public Long getSize() {
		return multipartFile.getSize();
	}
	
	public InputStream getInputStream() throws IOException {
		return multipartFile.getInputStream();
	}
	
	public boolean validLengthFileName() {
		String fileName = this.getFileName();
		return (fileName.length()>configuration.getMaxCharacters()) ? false : true;
	}
	
	public boolean validTypeFile() {
		String extension = this.getFileExtension();
		String[] extensiones = this.configuration.getValidatedExtensions();
		return (extension==null || extension.trim().isEmpty() || !Arrays.asList(extensiones).contains(extension.toLowerCase())) ? false : true;
	}

	
	public boolean validSizeFile() {
		Long size = this.getSize();
		return size > this.configuration.getMaxSize() ? false : true;
	}

	public void validateAll() throws Exception {
		if (!this.validLengthFileName()) {
			throw new CustomBussinessException(
					"Nombre de archivo muy extenso. Solo puede tener hasta "+this.configuration.getMaxCharacters()+" caracteres (incluido la extensión del archivo "+String.join(",", this.configuration.getValidatedExtensions())+").");
		}
		if (!this.validTypeFile()) {
			throw new CustomBussinessException(
					"Solo se permiten archivos con extensión "+String.join(",", configuration.getValidatedExtensions()));
		}
		if (!this.validSizeFile()) {
			throw new CustomBussinessException(
					"Tamaño de archivo no permitido. El límite de tamaño de archivo es de "+configuration.getMaxSize()+" MB.");
		}
	}

	public String getFileNameGenerated() {
		String prefix = "SISGLV";
		String uuid = UUID.randomUUID().toString().toUpperCase();
		String timestamp = this.getTimestampString();
		String ext = this.getFileExtension();
		String name = String.format("%s_%s_%s.%s", prefix, uuid, timestamp, ext);
		return name;
	}
	
	public String getUUID() {
		String prefix = "SISGLV";
		String uuid = UUID.randomUUID().toString().toUpperCase();
		String timestamp = this.getTimestampString();
		String name = String.format("%s_%s_%s", prefix, uuid, timestamp);
		return name;
	}
	
	private String getTimestampString() {
		Date date = Calendar.getInstance().getTime();  
        DateFormat dateFormat = new SimpleDateFormat("yyyymmddhhmmssms");  
        String strDate = dateFormat.format(date);
        return strDate;
	}
	
	public String getPath() {
		return Constantes.URL_SFTP;
	}
}
